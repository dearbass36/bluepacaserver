var id_formodal = 0;
$(function () {
    get_category("add");
    $('#key_add').tokenfield()
    $('#key_edit').tokenfield()
    $("#level1_add").change(function () {
        var c_id = $("#level1_add").val();
        $("#level2_add").html('<option value=" ">- LEVEL2 -</option>');
        $("#level3_add").html('<option value=" ">- LEVEL3 -</option>');
        $.ajax({
            url: "/get_productmenu",
            method: "GET",
            success: function (data) {
                var subc_txt = "";
                var count = 0;
                for (var j in data) {
                    if (data[j].c_level == 2 && c_id == data[j].c_subid) {
                        subc_txt += `<option value="${data[j].c_id}">${data[j].c_name_TH}</option>`;
                        count++;
                    }
                }
                if (count != 0) {
                    subc_txt = `<option value="">- LEVEL2 -</option>` + subc_txt;
                }
                else {
                    subc_txt = `<option value="">- LEVEL2 -</option>`
                }
                $("#level2_add").html(subc_txt);
            }
        });
    });
    $("#level2_add").change(function () {
        var subc_id = $("#level2_add").val();
        $.ajax({
            url: "/get_productmenu",
            method: "GET",
            success: function (data) {
                var subc_txt = "";
                for (var j in data) {
                    if (data[j].c_level == 3 && subc_id == data[j].c_subid) {
                        subc_txt += `<option value="${data[j].c_id}">${data[j].c_name_TH}</option>`;
                    }
                    else {
                        subc_txt = `<option value=" ">- LEVEL3 -</option>`
                    }
                }
                $("#level3_add").html(subc_txt);
            }
        });
    });
    $("#Upload_mainphoto").on('change', (function (e) {
        e.preventDefault();
        $.ajax({
            url: "/upload_mainphoto", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData: false,        // To send DOMDocument or non processed data file it is set to false
            success: function (data) {
                $("#main_img").val(data);
            }
        });
    }));
    $("#save_btn").click(function () {
        var param = {
            brand_EN: $("#brand_add_EN").val(),
            brand_TH: $("#brand_add_TH").val(),
            name_EN: $("#name_add_EN").val(),
            name_TH: $("#name_add_TH").val(),
            prop_EN: CKEDITOR.instances.prop_add_EN.getData(),
            prop_TH: CKEDITOR.instances.prop_add_TH.getData(),
            level1: $("#level1_add").val(),
            level2: $("#level2_add").val(),
            level3: $("#level3_add").val(),
            price: $("#price").val(),
            code: $("#code_add").val(),
            url: $("#url_add").val(),
            descript: $("#description_add").val(),
            keyword: $("#key_add").val(),
            main_img: $("#main_img").val(),
            sub_img: $("#sub_img").val(),
            discount: $("#discount").val(),
            pro_start: $("#date_start").val(),
            pro_end: $("#date_end").val(),
            status: $("#status_pro").val(),
        }
        $.ajax({
            url: "/add_product",
            method: "GET",
            data: param,
            success: function (response) {
                if (response.error != undefined) {
                    printErrorMsg(response.error);
                }
                else {
                    swal({
                        title: 'Success',
                        text: 'เพิ่มสินค้าเรียบร้อย !!',
                        type: 'success',
                        showConfirmButton: false
                    })
                    location.assign("/admin/product?id=" + response.subid);
                }
            }
        });
    });
    $(".edit_btn").click(function (event){
        event.stopPropagation();
        edit_click(this);
    });
    $(".del_btn").click(function (event){
        event.stopImmediatePropagation();
        Delproduct(this);
    })
    $(".lang_btn").click(function (event) {
        event.stopPropagation();
        lang_click(this);
    });
    $(".edit_back").click(function (event){
        event.stopPropagation();
        edit_back(this);
    });
    $(".edit_save").click(function (event){
        event.stopPropagation();
        edit_save(this);
    })
    $(".property_btn").click(function (event){
        event.stopPropagation();
        property_show(this);
    });
    $(".property_edit_btn").click(function (event){
        event.stopPropagation();
        property_edit(this);
    })
    $(".property_edit_back").click(function (event){
        event.stopImmediatePropagation();
        prop_edit_back(this);
    });
    $(".property_edit_save").click(function (event){
        event.stopImmediatePropagation();
        prop_edit_save(this);
    });
    $(".discount_btn").click(function (event){
        event.stopPropagation();
        discount_show(this);
    });
    $(".discount_save_btn").click(function (event){
        event.stopPropagation();
        discount_save(this);
    })
    $(".img_btn").click(function (event){
        event.stopPropagation();
        img_show(this);
    });
    $(".other_btn").click(function (event){
        event.stopPropagation();
        other_show(this);
    });
    $("#modal-upimg").on("submit" , function (event){
        event.stopPropagation();
        event.preventDefault();
        var form = new FormData(this);
        $("#modal_img").modal("hide");
        $.ajax({
            type: 'POST',
            url: '/editImgUpload',
            data: form,
            contentType: false,
            cache: false,
            processData:false,
            success: function(msg){
               $("#id_removeimg").val('');
               if(msg.img.length == 0){

               }
               else{
                   $("[data-id=" + msg.id +"]")
                   .find('img').attr('src','/picture/'+msg.img);       
               }
               
            }
        });
        swal({
            title: 'Success',
            text: 'แก้ไขรูปภาพเรียบร้อย !!',
            type: 'success',
            confirmButtonText: 'OK'
        })
    })
    $("#saveother_btn").click(function (){
        var param = {
            id : localStorage.getItem("id_formodal"),
            level1 : $("#level1_edit").val(),
            level2 : $("#level2_edit").val(),
            level3 : $("#level3_edit").val(), 
            url : $("#url_edit").val(),
            descript : $("#description_edit").val(),
            key : $("#key_edit").val(),
            status : $("#status_edit").val()
        }
        $.ajax({
            type: 'GET',
            url: '/editOther',
            data: param,
            success: function(msg){
                $("#modal_other").modal('hide');
               swal({
                    title: 'Success',
                    text: 'แก้ไขข้อมูลเรียบร้อย !!',
                    type: 'success',
                    confirmButtonText: 'OK'
                })
            }
        });
    })
    $(".level_edit").change(function (event){
        if($(this).val != ''){
            var param = {
                value : $(this).val(),
                level : $(this).data('level')
            }
            if(param.level == 1){
                $("[data-level=3]").html('<option value=" ">- LEVEL3 -</option>');
            }
            $.ajax({
                url : "/dynamiccategory",
                method : "GET",
                data : param ,
                success:function(response){
                    var place = parseInt(param.level)+1;
                    $("[data-level=" + place +"]").html(response);
                }
            });
        }
    })
    $('.modal').on('hidden.bs.modal', function () {
       $("#t_body").find('td').removeClass("row-focus");
    })
    $('.duplicate_btn').click(function (event){
        event.stopPropagation();
        duplicate_row(this);
    });
});
function printErrorMsg(msg) {
    $("#alert_msg").html('');
    var text = "";
    for (var i in msg) {
        text += `<div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>ผิดพลาด! </strong> ${msg[i]}
        </div>`;
    }
    $("#alert_msg").html(text);
}
function get_category(place) {
    if(place == "add"){
        $("#level1_add").html("<option>- LEVEL1 -</option>");
    }
    else{
        $("#level1_edit").html("<option>- LEVEL1 -</option>"); 
    }
    $.ajax({
        url: "/get_productmenu",
        method: "GET",
        success: function (data) {
            var c_txt = "";
            var subc_txt = "";
            for (var i in data) {
                if (data[i].c_level == 1) {
                    c_txt += `<option value="${data[i].c_id}">${data[i].c_name_TH}</option>`;
                }
            }
            if(place == "add"){
                $("#level1_add").append(c_txt);
            }
            else{
                $("#level1_edit").append(c_txt); 
            }
            
        }
    });
}
function Delproduct(el) {
    row_focus(el);
    swal({
        title: 'คุณแน่ใจที่จะลบ?',
        text: "หากลบแล้วไม่สามารถกู้ข้อมูลกลับมาได้",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ใช่,ฉันจะลบ',
        cancelButtonText: 'ยกเลิก'
      }).then((result) => {
        if (result.value) {
            var id = $(el).parents('tr').data().id;
            $.ajax({
                url: "/del_product",
                method: "GET",
                data: {id : id},
                success: function (data) {
                    $(el).parents('tr').remove();
                }
            });
          swal({
            title :  'ลบเรียบร้อย!',
            text: 'สินค้าของคุณได้ถูกลบเรียบร้อย.',
            type: 'success',
            confirmButtonColor: 'green',
          })
        }
        else {
            $(el).parents('tr').children('td').removeClass('row-focus');
        }
      })
}
function readURL(input) {
    var Parent = $(input).parent('form').children('img');
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(Parent).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input){
    var Parent = $(input).parent('div').children('img');
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(Parent).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    } 
}
function lang_click(el){
        var ParentDIV = $(el).parents('div'),
            childP = ParentDIV.children('p'),
            childA = ParentDIV.children('a'),
            lang =   $(el).data().lang;

        childP.hide();
        childA.removeClass('active');
        childP.each(function (index, el) {
            if ($(el).data().lang == lang) {
                $(el).show();
            }
        });
        childA.each(function (index, el) {
            if ($(el).data().lang == lang) {
                $(el).addClass('active');
            }
        });
}
function edit_click(el){
    row_focus(el);
        var  ChildTD = $(el).parents('tr');
        ChildTD.find('.wrap-option').show();
        ChildTD.find('.wrap-control').hide();
}
function get_proterty(id_product){
    $.ajax({
        url: "/product_byid",
        method: "GET",
        data: {id:id_product},
        success: function (data) {
            $("#property_TH").html(data[0].p_detail_TH);
            $("#property_EN").html(data[0].p_detail_EN);
            $("#modal_property").modal('show');
        
        }
    });
}
function get_discount(id_product){
    $.ajax({
        url: "/product_byid",
        method: "GET",
        data: {id:id_product},
        success: function (data) {
            $("#modal_discount").modal('show');
        
        }
    });
}
function edit_back(el){
        var ParentTR = $(el).parents('tr');
        ParentTR.find('p').each(function (index,el){
             var i = index;
             var value = $(el).html();
            ParentTR.find('input').each(function (index,el){
                if(i == index){
                    $(el).val(value)
                }
            });
        });
        ParentTR.find('.wrap-option').hide();
        ParentTR.find('.wrap-control').show();
        ParentTR.children('td').removeClass('row-focus');
}
function edit_save(el){
       var ParentTR = $(el).parents('tr');
            var param = {
                id : ParentTR.data().id,
                brand_TH : ParentTR.find(".brandTH_edit").val(),
                brand_EN : ParentTR.find(".brandEN_edit").val(),
                name_TH :  ParentTR.find(".nameTH_edit").val(),
                name_EN :  ParentTR.find(".nameEN_edit").val(),
                price :    ParentTR.find(".price_edit").val(),
                amount :   ParentTR.find(".amount_edit").val()
            }
            if(parseInt(param.price)+10 && parseInt(param.amount)+10){
               $.ajax({
                url: "/edit_mainproduct",
                method: "GET",
                data: param,
                success: function (data) { 
                        swal({
                            title: 'Success',
                            text: 'แก้ไขข้อมูลเรียบร้อย !!',
                            type: 'success',
                            confirmButtonText: 'OK'
                        })
                    }
                });
                ParentTR.find(".brandTH_txt").html(param.brand_TH);
                ParentTR.find(".brandEN_txt").html(param.brand_EN);
                ParentTR.find(".nameTH_txt").html(param.name_TH);
                ParentTR.find(".nameEN_txt").html(param.name_EN);
                ParentTR.find(".price_txt").html(param.price);
                ParentTR.find(".amount_txt").html(param.amount);
                ParentTR.find('.wrap-option').hide();
                ParentTR.find('.wrap-control').show();
                ParentTR.children('td').removeClass('row-focus');
            }
            else{
                swal({
                    title: 'Failed',
                    text: 'ชนิดข้อมูลผิดพลาด !!',
                    type: 'error',
                    confirmButtonText: 'OK'
                })
            }
        
}
function property_show(el){
    row_focus(el);
    var id = $(el).parents('tr').data().id;
    var data = product_byid(id);
    id_formodal = id;
    $("#propertyTH_edit_text").html(data.p_detail_TH);
    $("#propertyEN_edit_text").html(data.p_detail_EN);
    CKEDITOR.instances.propTH_edit_area.setData(data.p_detail_TH)
    CKEDITOR.instances.propEN_edit_area.setData(data.p_detail_EN)
    $("#modal_property").modal("show");
}
function property_edit(el){
    var Parent = $(el).parent('div').parent('div');
    Parent.find('.wrap-control').hide();
    Parent.find('.wrap-option').show();
}
function prop_edit_back(el){
    var Parent = $(el).parents('div');
    Parent.find('.wrap-control').show();
    Parent.find('.wrap-option').hide();
    var p_TH = $("#propertyTH_edit_text").html();
    var p_EN = $("#propertyEN_edit_text").html();
    CKEDITOR.instances.propTH_edit_area.setData(p_TH)
    CKEDITOR.instances.propEN_edit_area.setData(p_EN)
}
function prop_edit_save(el){
    row_focus(el);
    var param = {
        id : id_formodal,
        prop_TH : CKEDITOR.instances.propTH_edit_area.getData(),
        prop_EN : CKEDITOR.instances.propEN_edit_area.getData()
    }
    $.ajax({
        url: "/edit_prop",
        method: "GET",
        data: param,
        success: function (data) { 
            $("#propertyTH_edit_text").html(param.prop_TH);
            $("#propertyEN_edit_text").html(param.prop_EN);
            CKEDITOR.instances.propTH_edit_area.setData(param.prop_TH)
            CKEDITOR.instances.propEN_edit_area.setData(param.prop_EN)
            
            var Parent = $(el).parents('div');
            Parent.find('.wrap-control').show();
            Parent.find('.wrap-option').hide();
            swal({
                title: 'Success',
                text: 'แก้ไขคุณสมบัติเรียบร้อย !!',
                type: 'success',
                confirmButtonText: 'OK'
            })
        }
    });
    
}
function discount_show(el){
    row_focus(el);
    var id = $(el).parents('tr').data().id;
    id_formodal = id;
    $.get('/get_promotion',{ id : id_formodal },function(response){
        $("#start_edit_text").val(response.pro_datestart);
        $("#end_edit_text").val(response.pro_dateend);
        $("#discount_edit_text").val(response.discount);
    });
   $("#modal_discount").modal("show");
}
function discount_save(el){
    var param = {
        id : id_formodal,
        discount : $("#discount_edit_text").val(),
        date_start : $("#start_edit_text").val(),
        date_end : $("#end_edit_text").val()
    }
    $.get('/save_discount', param , function(response){
        if(response.success == 'done'){
            $("#modal_discount").modal('hide');
            swal({
                title : 'สำเร็จ',
                text : 'อัพเดทโปรโมชั่นสำเร็จ',
                type : 'success'
            })
        }
        else{
            swal({
                title : 'ล้มเหลว',
                text : 'อัพเดทโปรโมชั่นล้มเหลว',
                type : 'error'
            })
        }
    });
}
function img_show(el){
    row_focus(el);
    var id = $(el).parents('tr').data().id;
    $("#id_modalimg").val(id);
    var data = product_byid(id);
    var subimg = getsubimg_byid(id);
    $("#img_main").html('');
    $("#img_sub").html('');
    $("#id_removeimg").val('');
    $("#img_main").append(`<img id="mainmodal_img" src="/picture/${data.p_img}" alt="your image" width="230" height="180" onclick="mainmodal_file.click()" />
                            <br><br>
                            <input type="file" class="form-control" name="main_file" id="mainmodal_file" value="" onchange="readURL2(this);" style="display:none"> 
                            <span ><a href="javascript:;" style="color:orange;" onclick="mainmodal_file.click()">*คลิกเพื่ออัพโหลด</small></a>`);
    for(var i in subimg){
        var text = `<div class="gallery" data-id="${subimg[i].subimg_id}">
                        <img class="blah" src="/images/${subimg[i].subimg_name}" width="60px" height="60px" onclick="subimg_input_${i}.click()">
                        <div class="desc"><a class="remove_subimg_btn" onclick="remove_subimg(this)">Remove</a></div>
                    </div>`;
        $("#img_sub").append(text);
    }
    $("#modal_img").modal("show"); 
}
function add_subimg(el){
   var countDiv = $("#img_sub").find('.gallery').length;
   var id = countDiv;
   var text = `
                    <div class="gallery">
                        <img class="blah" src="/picture/new_subimg.jpg" width="60px" height="60px" onclick="subimg_input_${id}.click()">
                        <input type="file" class="form-control inputfile_edit" name="subimg[]" id="subimg_input_${id}" onchange="readURL2(this);" style="display:none">
                        <div class="desc"><a class="remove_subimg_btn" onclick="remove_subimg(this)">Remove</a></div>
                    </div>
               `;
    $("#img_sub").append(text);
}
function remove_subimg(el){
    var Parent = $(el).parents('div.gallery');
    var id_remove = Parent.data().id;
    var old_idremove = $("#id_removeimg").val();
    
        if(old_idremove.length==0){
        
            $("#id_removeimg").val( id_remove );
        }
        else{
            $("#id_removeimg").val(old_idremove + "," + id_remove );
        }
    Parent.remove();
}
function other_show(el){
    row_focus(el);
    var id = $(el).parents('tr').data().id;
    localStorage.setItem("id_formodal",id);
    id_formodal = id;
    get_category("edit");
    var id = $(el).parents('tr').data().id;
    var data = product_byid(id);
                $.ajax({
                        url: "/category_edit",
                        method: "GET",
                        data: {c_id : data.c_id},
                        success:function (response){
                            $("#level3_edit").html(response[0][2]);
                            $("#level2_edit").html(response[0][1]);
                            $("#level1_edit").html(response[0][0]);
                            if(response[1][2]!=null){
                                $("#level3_edit").val(response[1][2]);
                                $("#level2_edit").val(response[1][1]);
                                $("#level1_edit").val(response[1][0]);
                            }
                            else if(response[1][1]!=null && response[1][2]==null){
                                $("#level2_edit").val(response[1][1]);
                                $("#level1_edit").val(response[1][0]);
                            }
                            else{
                                $("#level1_edit").val(response[1][0]);
                            }
                        }
                });
    $("#url_edit").val(data.p_url);
    $("#description_edit").val(data.p_description);
    $("#key_edit").tokenfield('setTokens',data.p_keyword);
    $("#status_edit").val(data.p_status);
    $("#modal_other").modal('show');
}
function product_byid(id){
    var result = "";
                $.ajax({
                        url: "/product_byid",
                        method: "GET",
                        data: {id : id},
                        async: false,
                        success:function (data){
                           result = data
                        }
                });
    return result[0];
}
function getsubimg_byid(id){
    var result = "";
                $.ajax({
                        url: "/getsubimg_byid",
                        method: "GET",
                        data: {id : id},
                        async: false,
                        success:function (data){
                           result = data
                        }
                });
    return result; 
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function reloadpage() {
    await sleep(1000);
    location.reload();
}
function row_focus(el){
    var Parent = $(el).parents('tr').children('td');
    Parent.each(function( index, el){
        $(el).addClass('row-focus');
    });
}
async function duplicate_row(el){
    row_focus(el);
    var Parent = $(el).parents('tr');
    var id = Parent.data().id;
    var ChildTD = Parent.children('td');
    const {value: code} = await swal({
        title: 'กรุณาใส่รหัสสินค้า',
        input: 'text',
        inputPlaceholder: 'รหัสสินค้า',
        showCloseButton: true,
        inputValidator: (code) => {
            return !code && 'กรุณาใส่รหัสสินค้า'
          }

      })
      if (code) {
          if(isNaN(code) == true){
            swal({
                title : 'ล้มเหลว' ,
                text : 'กรุณาใส่รหัสสินค้าเป็นตัวเลข',
                type : 'error',
                })
          }
          else{
              var param = {
                  id : id,
                  code : code
              }
              $.ajax({
                url : '/save_duplicate',
                type : 'GET',
                data : param ,
                success : function(response){
                    if(response.success==null){
                        //reloadpage();
                        Add_row(response,id)
                        swal({
                            title : 'เสร็จสิ้น ' ,
                            text : 'คัดลอก ' +  code  + ' สำเร็จ',
                            type : 'success',
                            })
                    }
                    else{
                        swal({
                            title : 'ล้มเหลว' ,
                            text : 'คัดลอกไม่สำเร็จ , รหัสสินค้าซ้ำ',
                            type : 'error',
                            })  
                    }
                }
              });
          }
      }
      ChildTD.removeClass('row-focus');
}
function Add_row(data,id){
    var row = $("[data-id="+id+"]");
    var clone = `<tr class="odd gradeX" data-id="${data.id}">
                    <td class="with-img text-center">
                        <img src="/picture/${data.p_img}" class="img-rounded" height="70px" width="100"/>
                    </td>
                    <td data-row="code"><span>${data.p_code}</span> &nbsp;<a class="duplicate_btn" onclick="duplicate_row(this)" data-toggle="tooltip" title="คัดลอก"><i class="fas fa-copy fa-lg"></i></a></td>
                    <td data-row="brand" class="text-center">
                        <div class="wrap-control">
                            <p data-lang="TH" class="brandTH_txt">${data.p_brand_TH}</p>
                            <p data-lang="ENG" class="brandEN_txt" style="display:none;">${data.p_brand_EN}</p>
                            <a data-lang="TH" onclick="lang_click(this)" class="btn btn-xs btn-icon btn-circle btn-default lang_btn active">TH</a>
                            <a data-lang="ENG" onclick="lang_click(this)" class="btn btn-xs btn-icon btn-circle btn-default lang_btn">ENG</a>   
                        </div>
                        <div class="wrap-option">
                            <input type="text" class="form-control brandTH_edit input-table" placeholder="แบรนด์" value="${data.p_brand_TH}" >
                            <input type="text" class="form-control brandEN_edit input-table" placeholder="brand" value="${data.p_brand_EN}" >    
                        </div>
                    </td>
                    <td data-row="name" class="text-center">
                        <div class="wrap-control">
                            <p data-lang="TH" class="nameTH_txt">${data.p_name_TH}</p>
                            <p data-lang="ENG"  class="nameEN_txt" style="display:none;">${data.p_name_EN}</p>
                            <a data-lang="TH"  onclick="lang_click(this)" class="btn btn-xs btn-icon btn-circle btn-default lang_btn active">TH</a>
                            <a data-lang="ENG" onclick="lang_click(this)" class="btn btn-xs btn-icon btn-circle btn-default lang_btn">ENG</a>
                        </div>
                        <div class="wrap-option">
                            <input type="text" class="form-control nameTH_edit input-table"  placeholder="ชื่อสินค้า" value="${data.p_name_TH}" ><br>
                            <input type="text" class="form-control nameEN_edit input-table"  placeholder="Product Name" value="${data.p_name_EN}" >
                        </div>
                        
                    </td>
                    <td data-row="price">
                        <div class="wrap-control">
                            <p class="price_txt">${data.p_price}</p>
                        </div>
                        <div class="wrap-option">
                            <input type="text" class="form-control price_edit" value="${data.p_price}" size="5">
                        </div>
                    
                    </td>
                    <td data-row="amount">
                        <div class="wrap-control">
                            <p class="amount_txt">${data.p_amount}</p>
                        </div>
                        <div class="wrap-option">
                            <input type="text" class="form-control amount_edit" value="${data.p_amount}" size="5">
                        </div>
                    
                    </td>
                    <td>
                        <br>
                            <div class="wrap-control">
                                <a  class="edit_btn" onclick="edit_click(this)"><i class="fas fa-pencil-alt fa-fw"></i></a>
                                <a  class="del_btn" onclick="Delproduct(this)"><i class="fas fa-trash-alt fa-fw"></i></a>
                            </div>
                            <div class="wrap-option">
                                <a  class="edit_save green" onclick="edit_save(this)"><i class="fas fa-check fa-fw"></i></a>
                                <a  class="edit_back red" onclick="edit_back(this)"><i class="fas fa-times fa-fw"></i></a>
                            </div>
                            
                            <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown"><i class="fas fa-cog fa-fw"></i></a>
                            <ul class="dropdown-menu pull-right">
                                <li><a class="property_btn" onclick="property_show(this)"><i class="fas fa-sticky-note fa-fw"></i> คุณสมบัติ</a></li>
                                <li><a class="discount_btn" onclick="discount_show(this)"><i class="fas fa-money-bill-alt fa-fw"></i> &nbsp;ส่วนลด</a></li>
                                <li><a class="img_btn" onclick="img_show(this)"><i class="fas fa-image fa-fw"></i> รูปภาพ</a></li>
                                <li><a class="other_btn" onclick="other_show(this)"><i class="fas fa-info-circle fa-fw"></i> อื่นๆ</a></li>
                            </ul>
                    </td>
                </tr>`;
    $(clone).insertAfter(row);                
}

