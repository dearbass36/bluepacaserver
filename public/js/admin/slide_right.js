var id_ForCreateCategory;
$.ajaxSetup({
    headers : {
        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
    }
});
$(function () {
    $("#managepro_menu").click(function (event){
        event.stopPropagation();
    });
    $.ajax({
        url : "/checkid_cat",
        type : "GET",
        success : function(response){
            id_ForCreateCategory = response.id;
        }
    });
    $(".main-level").find('ul').each(function(index,el){
        $(el).hide();
    });
    var delay = 350 , setTimeoutConst;
    $(".main-level").hover(
        function(){
            var ManuThis = $(this);
            setTimeoutConst = setTimeout(function(){
                ManuThis.children('ul').slideDown();
            },delay) 
        },
        function(){
            $(this).find('ul').slideUp();
            clearTimeout(setTimeoutConst)
        }
    );
    $("#managepro_menu").hover(
        function(){
            var ManuThis = $(this);
            setTimeoutConst = setTimeout(function(){
                ManuThis.children('ul').slideDown();
            },delay) 
        },
        function(){
            $(this).find('ul').slideUp();
            clearTimeout(setTimeoutConst)
        }
    );
    $(".level1").hover(
        function(){
            var ManuThis = $(this);
            setTimeoutConst = setTimeout(function(){
                ManuThis.children('ul').slideDown();
            },delay);
        }, function(){
            clearTimeout(setTimeoutConst)
    });
    $(".level2").hover(
        function(){
            var ManuThis = $(this);
            setTimeoutConst = setTimeout(function(){
                ManuThis.children('ul').slideDown();
            },delay);
        }, function(){
            clearTimeout(setTimeoutConst)
    });
    
    $("[data-catid]").click(function(event){
        event.stopPropagation();
        event.preventDefault();
        var id = $(this).data().catid;
        window.location.replace("/admin/product?id="+id);
    });
    $(".catagoryadd_btn").click(function(){
        $.ajax({
            url :'/get_productmenu' ,
            type : 'GET',
            success : function (response){
                var data = [];
                var value = { "id" : "0", "parent" : "#", "text" : "Category" , "type" : "#" , "state" : {opened : true , selected : true}}
                    data.push(value)
                for(var i in response){
                    var parent , type;

                    if(response[i].c_level == 1){
                        parent = "0"
                        type = "child1" ;
                    }
                    else if(response[i].c_level == 2){
                        parent = response[i].c_subid;
                        type = "child2" ;
                    }
                    else if(response[i].c_level == 3){
                        parent = response[i].c_subid;
                        type = "child3";
                    }
                    var value = { "id" : String(response[i].c_id), "parent" : parent, "text" : response[i].c_name_TH  , "type" : type , 
                                  "data" : {"text_en" : response[i].c_name_EN}
                                }
                    data.push(value);
                }
                var handleJstreeDragAndDrop = function() {
                    $("#jstree-catagory").jstree({
                        "core": {
                            "themes": {
                                "responsive": true,
                                "variant" : "large"
                                
                            }, 
                            "check_callback": true,
                            'data': data
                        },
                        "types": {
                            "#" : {
                                "valid_children" : ["child1"],
                                "icon" : "fa fa-folder text-disable fa-lg"
                            },
                            "child1": {
                                "valid_children" : ["child2"],
                                "icon" : "fa fa-folder text-warning fa-lg"
                            },
                            "child2": {
                                "valid_children" : ["child3"],
                                "icon" : "fa fa-folder text-primary fa-lg"
                            },
                            "child3": {
                                "valid_children" : [],
                                "icon" : "fa fa-folder text-danger fa-lg"
                            }
                        },
                        "state": { "key": "demo2" },
                        "plugins": ["types"  , "wholerow" ,"dnd"]
                    });
                };
                var TreeView = function () {
                    "use strict";
                    return {
                        //main function
                        init: function () {
                            handleJstreeDragAndDrop();
                        }
                    };
                }();
                TreeView.init()
            }
        });
        $("#catagoryadd_modal").modal('show');
    });
    $('#catagoryadd_modal').on('hidden.bs.modal', function () {
        treeSQL = [];
        $("#body_rename").hide();
        $("#body_create").hide();
     })
    $('.cancel_cat_btn').click(function (){
        $("#body_rename").hide();
        $("#body_create").hide();
    });
    $("#jstree-catagory").bind(
        "select_node.jstree", function(evt, data){
            $("#body_create").hide();
            $("#body_rename").hide();
            $("#createTH_text").val('');
            $("#createEN_text").val('');
        }
);
});
function create_cat() {
    var ref = $('#jstree-catagory').jstree(true),
							sel = ref.get_selected("full",true),type;
							if(!sel.length || sel[0].type == "child3") { return false; }
                            sel = sel[0];
                            $("#body_create").show();
                            $("#body_rename").hide();
                            var v = $('#jstree-catagory').jstree(true).get_json('#', {flat:true});
                            var parent = sel.parents;
                            var length = parent.length;
                            var text = "";
                            if(length == 2){
                                text += "Category / ";
                            }
                            else if(length == 3){
                                for(var i in v){
                                    if(v[i].id == parent[0]){
                                        text += "Category /" + v[i].text + " / ";
                                    }
                                }
                            }
                            text += sel.text;
                            $("#level_text").html(text); 
                            $(".create_conBTN").prop('disabled', true);
                            $(".create_text").keyup(function(){
                                var TH = $("#createTH_text").val();
                                var EN = $("#createEN_text").val();
                                if(TH.length == 0 || EN.length == 0){
                                    $(".create_conBTN").prop('disabled', true);
                                }else{
                                    $(".create_conBTN").prop('disabled', false);
                                }  
                            });
                                           
};
function rename_cat() {
    $("#body_create").hide();
    var ref = $('#jstree-catagory').jstree(true),
        sel = ref.get_selected("full",true);
    if(!sel.length || sel[0].text == "Category") { return false; } 
    $("#body_rename").show();
    sel = sel[0];
    $("#renameTH_text").val(sel.original.text);
    $("#renameEN_text").val(sel.data.text_en);
    $("#rename_conBTN").click(function (){
        var th = $("#renameTH_text").val();
        var en = $("#renameEN_text").val();
        sel.data.text_en = en;
        ref.rename_node(sel,th);
        $("#body_rename").hide();
    });
    //ref.edit(sel);
};
function delete_cat() {
    var ref = $('#jstree-catagory').jstree(true),
        sel = ref.get_selected("full",true);
    if(!sel.length || sel[0].text == "Category") { return false; }             
    var ref = $('#jstree-catagory').jstree(true),
        sel = ref.get_selected("full",true);
    if(!sel.length) { return false; }
        ref.delete_node(sel);
};
function crate_click(){
    var ref = $('#jstree-catagory').jstree(true),
                            sel = ref.get_selected("full",true),type;
                            type = "child1";
                            sel = sel[0];
                            if(sel.parents.length == 2){
                                type = "child2";
                            }
                            else if(sel.parents.length == 3){
                                type = "child3";
                            }
                            sel.state.opened = true;
                            id_ForCreateCategory++;
                                var param = {
                                    id : id_ForCreateCategory,
                                    type : type,
                                    text : $("#createTH_text").val(),
                                    data : {text_en : $("#createEN_text").val()},
                                }
                                sel = ref.create_node(sel, param);
                                $("#body_create").hide();
                                $("#createTH_text").val('');
                                $("#createEN_text").val('');
}
function save_cat(){
    var v = $('#jstree-catagory').jstree(true).get_json('#', {flat:true});
    var param = [];
    for(var i in v){
        var level = 1;
        if(v[i].type == "child2"){
            level = 2;
        }
        else if(v[i].type == "child3"){
            level = 3;
        }
        var vl = { "c_id" : parseInt(v[i].id) , "c_name_TH" : v[i].text , "c_name_EN" : v[i].data.text_en , "c_level" : level, "c_subid" : v[i].parent }
        param.push(vl);
    }
    $.ajax({
        url : "/save_cat",
        data : {data : JSON.stringify(param)},
        type : "GET",
        success : function (response){
            location.reload();
        }
    });
};


