<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flashsale extends Model
{
    //
    public $timestamps = false;
    protected $table = 'flashsales';
}
