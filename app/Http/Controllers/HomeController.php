<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\GeneratorInterface;
use DB;
use Validator;
use App\Product;
use App\sub_img;
use App\Order;
use App\Category;
use App\notification;
use App\Coupons;
use App\Flashsale;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function home(){
        $catagory = $this->GetProductmenu();
        $notic = $this->GetNoticfication();
        $order = Order::join('users','orders.owner_id','=','users.id')->get();
        $none = Order::where('order_status','ชำระเงินแล้ว')->Orwhere('order_status','จัดส่งพัสดุแล้ว')->get();
        $countnone = $none->count();
        $countorder = $order->count();
        if($countorder != 0){
            $orderAVG = 100-round(((($countorder-$countnone)/$countorder) * 100),2);
        }
        else{
            $orderAVG = 0 ;
        }
        
        $product = Product::all();
        $countproduct = $product->count();
        return view('admin.page.home',['catagory'=>$catagory,'notic'=>$notic,'order'=>$order,'countorder'=>$countorder,'countproduct'=>$countproduct,'orderAVG'=>$orderAVG]);
    }
    public function order(){
        $catagory = $this->GetProductmenu();
        $notic = $this->GetNoticfication();
        $order = Order::join('users','orders.owner_id','=','users.id')->get();
        $order = Order::join('users','orders.owner_id','=','users.id')->orderBy('orders.order_date','DESC')->get();
        return view('admin.page.order',['catagory'=>$catagory,'notic'=>$notic,'order'=>$order]);
    }
    public function product(Request $request){
        $id = $request->get('id');
        if($id == "all"){
            $Head = "";
            $product = Product::all();
        }
        else{
            $detail = Category::where('c_id',$id)->get();
            $Head = " - ". $detail[0]['c_name_TH'];
            $isLevel = $detail[0]['c_level'];
            if($isLevel == "1"){
                $array_id = array();
                array_push($array_id,$id);
                $catagorylevel2 = Category::where('c_id',$id)->get();
                foreach($catagorylevel2 as $row){
                    array_push($array_id,$row->c_id);
                    $catagorylevel3 = Category::where('c_id',$row->c_id)->get();
                    foreach($catagorylevel3 as $row2){
                        array_push($array_id,$row2->c_id);
                    }
                }
                $product = Product::whereIn('c_id',$array_id)->get();
            }
            else if($isLevel == "2"){
                $array_id = array();
                array_push($array_id,$id);
                $catagorylevel2 = Category::where('c_id',$id)->get();
                foreach($catagorylevel2 as $row){
                    array_push($array_id,$row->c_id);
                }
                $product = Product::whereIn('c_id',$array_id)->get();
            }
            else {
                $product = Product::where('c_id',$id)->get();
            }
        }
        $catagory = $this->GetProductmenu();
        $notic = $this->GetNoticfication();
        $order = Order::join('users','orders.owner_id','=','users.id')->get();
        return view('admin.page.product',['catagory'=>$catagory,'notic'=>$notic,'product'=>$product,'head'=>$Head]);
    }
    public function promotion(){
        $catagory = $this->GetProductmenu();
        $notic = $this->GetNoticfication();
        return view('admin.page.promotion',['catagory'=>$catagory,'notic'=>$notic,'coupons'=>$coupons]);
    }
    public function coupons(){
        $catagory = $this->GetProductmenu();
        $notic = $this->GetNoticfication();
        $coupons = Coupons::orderby("cp_id", 'DESC')->get();
        return view('admin.page.coupons',['catagory'=>$catagory,'notic'=>$notic,'coupons'=>$coupons]);
    }
    public function flashsale(){
        $catagory = $this->GetProductmenu();
        $notic = $this->GetNoticfication();
        $flashsale = Flashsale::orderby("fs_id", 'DESC')->get();
        return view('admin.page.flashsale',['catagory'=>$catagory,'notic'=>$notic,'flashsale'=>$flashsale]);
    }
    public function blogManage(){
        $catagory = $this->GetProductmenu();
        $notic = $this->GetNoticfication();
        return view('admin.page.blogManage',['catagory'=>$catagory,'notic'=>$notic]);
    }
    public function GetProductmenu(){
        $output = "";
        $level1 = Category::where('c_level',1)->get();
        $level2 = Category::where('c_level',2)->get();
        $level3 = Category::where('c_level',3)->get();
        foreach($level1 as $row1){
            $output .= "<li class='level1'><a data-catid='".$row1->c_id."'>".$row1->c_name_TH."</a><ul class='sub-menu'>";
            foreach($level2 as $row2){
                if($row2->c_subid == $row1->c_id){
                    $output .= "<li class='has-sub level2'><a data-catid='".$row2->c_id."'>".$row2->c_name_TH."</a><ul class='sub-menu level3'>";
                    foreach($level3 as $row3){
                        if($row3->c_subid == $row2->c_id){
                            $output .= "<li><a data-catid='".$row3->c_id."'>".$row3->c_name_TH."</a></li>";
                        }
                    }
                    $output .= "</ul></li>";
                }
            }
            $output .= "</ul></li>";
        }
        $output .= "<li class=''><a class='catagoryadd_btn'><i class='fa fa-pencil-alt'></i>&nbsp;จัดการหมวดหมู่</a></li>";
        return $output;
    }
    public function GetNoticfication(){
        return  notification::join('orders','notifications.order_id','=','orders.order_id')
                                    ->join('users','orders.owner_id','=','users.id')
                                    ->orderBy('notifications.notic_date','DESC')->get();
    }
    public function checkid_cat(){
       $check_id = Category::orderBy('c_id','desc')->first();
       if($check_id == null){
            return response()->json(['id' => 0]);
       }
       else{
            return response()->json(['id' => $check_id->c_id]);
       }
       
    }
    public function save_cat(Request $request){
            $data = json_decode($request->get('data'));
            $del_cat = Category::truncate();
            foreach( $data as $row ){
                if($row->c_id == 0){

                }
                else{
                    $Category = new Category;
                    $Category->c_id = $row->c_id;
                    $Category->c_name_TH = $row->c_name_TH;
                    $Category->c_name_EN = $row->c_name_EN;
                    $Category->c_level = $row->c_level;
                    $Category->c_subid = $row->c_subid;
                    $Category->timestamps = false;
                    $Category->save(); 
                }
               
            }
            return response()->json(['success','done']);
    }
    public function testnanoid(){
        $client = new Client();
        echo $client->generateId($size = 21, $mode = Client::MODE_DYNAMIC);
    }
}
