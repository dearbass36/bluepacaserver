<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
    //
    public $timestamps = false;
    protected $table = 'coupons';

    protected $fillable = [
       'cp_code','cp_discount'
    ];
}
