<?php
use App\Category;
use App\Product;
use App\sub_img;
use App\Promotion;
use App\Order;
use App\Orderdetail;
use App\notification;
use App\Coupons;
use App\Flashsale;
use Carbon\Carbon;;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within 

group which
| contains the "web" middleware group. Now create something great!
|
*/

/////////// Login & Default Page /////////////////
Route::get('/' , 'HomeController@home');
Route::get('/login', function () {
    return view('auth.login');
});
Route::get('/logout', function (){Auth::logout();return view('auth.login');}); 

Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin/home','HomeController@home');
    Route::get('/admin/order','HomeController@order');
    Route::get('/admin/product','HomeController@product');
    Route::get('/admin/promotion','HomeController@promotion');
    Route::get('/admin/coupons','HomeController@coupons');
    Route::get('/admin/flashsale','HomeController@flashsale');
    Route::get('/admin/blog_manage','HomeController@blogManage');
});
Route::get('/checkid_cat','HomeController@checkid_cat');
Route::get('/save_cat','HomeController@save_cat');
Route::get('/testnanoid','HomeController@testnanoid');
Route::get('/get_notic',function(){
    $notic = notification::join('orders','notifications.order_id','=','orders.order_id')
                                ->join('users','orders.owner_id','=','users.id')
                                ->orderBy('notifications.notic_date','DESC')->get();
    $order = Order::join('users','orders.owner_id','=','users.id')->get();
   
    return $notic;
});
Route::get('/update_notic',function(){
    $update_notic = notification::where('notic_status' , 0)->update(['notic_status' => 1]);
});
Route::get('/update_notic_read',function(Request $request){
    $update_notic = notification::where('notic_id' , $request->get('id'))->update(['notic_status' => 2]);
});
Route::get('/notification', function(){
    $notic = notification::join('orders','notifications.order_id','=','orders.order_id')
                                ->join('users','orders.owner_id','=','users.id')
                                ->orderBy('notifications.notic_date','DESC')->get();
        $order = Order::join('users','orders.owner_id','=','users.id')->get();
   
    return view('admin.page.notification',['notic'=>$notic]);
});
Route::get('/posts', function(Request $request){
    $id = $request->get('id');
    $notic = notification::join('orders','notifications.order_id','=','orders.order_id')
                                ->join('users','orders.owner_id','=','users.id')
                                ->orderBy('notifications.notic_date','DESC')->get();
    $this_order = Order::where('order_id',$id)->get();
    $this_detail = Orderdetail::join('products', function ($join) use ($id){
                                    $join->on('orderdetails.p_id', '=', 'products.p_id')
                                    ->where('orderdetails.order_id',$id);
                                })->get();
    return view('admin.page.post',['notic'=>$notic,'order'=>$this_order,'detail'=>$this_detail,'']);
});
//////////////// End Login & Default Page ///////////////////

/////////////////// Admin Product ////////////////////////////

Route::get('/get_productmenu', function () {
    $category = Category::all()->toArray();
    return $category;
});
Route::get('add_product', ['as'=>'add_product','uses'=>'ProductController@add_product']);
Route::get('get_allproduct', function (){
    $allproduct = Product::all();
    return $allproduct;
});
Route::get('del_product',function (Request $request){
    Product::where('p_id',$request->query('id'))->delete();
    sub_img::where('p_id',$request->query('id'))->delete();
    return response()->json(['msg'=>'done']);
});
Route::get('/admin/product_add', function (){
    return view('admin.page.product_add');
});
Route::post('/upload','ProductController@multiupload'); //////multi upload sub img//////
Route::get('/delete_photo','ProductController@delete_photo');//////Del multi upload sub img//////
Route::post('upload_mainphoto','ProductController@upload_mainphoto');////// upload main img ///////..
Route::get('product_byid',function (Request $request){
    $data = Product::where('p_id',$request->query('id'))->get();
    return $data;
});
Route::get('getsubimg_byid',function (Request $request){
    $subimg = sub_img::where('p_id',$request->query('id'))->get();
    return $subimg;
});
Route::get('/edit_mainproduct','ProductController@edit_mainproduct');
Route::get('/edit_prop','ProductController@edit_prop');
Route::post('editImgUpload',['as'=>'editImgUpload','uses'=>'ProductController@editImgUpload']);
Route::get('dynamiccategory','ProductController@dynamiccategory');
Route::get('editOther','ProductController@editOther');
Route::get('category_edit','ProductController@category_edit');
Route::get('save_duplicate','ProductController@save_duplicate');
Auth::routes();
Route::get('test','ProductController@GetProductmenu');
Route::get('get_promotion',function(Request $request){
    $promotion = Promotion::where('p_id',$request->get('id'))->get();
    if(count($promotion) != 0){
        $promotion = $promotion[0];
        return response()->json(['discount' => $promotion->p_discount , 'pro_datestart' => $promotion->pro_datestart , 'pro_dateend' => $promotion->pro_dateend]);
    }
    else {
        return response()->json(['sucess' => 'empty']);
    }
});
Route::get('save_discount','ProductController@save_discount');
/////////////////// End Admin Product /////////////////////////


/////////////////// Mock Page //////////////////////
Route::get('/Mock',function(){
    $product = Product::all();
    if(Auth::user()->id){
        $id = Auth::user()->id;
        $od = Orderdetail::join('products', function ($join) use ($id){
                                    $join->on('orderdetails.p_id', '=', 'products.p_id')
                                    ->where('orderdetails.owner_id',$id)
                                    ->where('orderdetails.od_status',0);
                                })->get();
        return view('user.page.mockPage',['product' => $product , 'od' => $od]);
    }
    else{
        return view('user.page.mockPage',['product' => $product]);
    }
    
});

Route::get('/user/orderpage', function(){
    $id = Auth::user()->id;
    $orderall = Order::where('owner_id',$id)->get();
    $od = Orderdetail::join('products', function ($join) use ($id){
        $join->on('orderdetails.p_id', '=', 'products.p_id')
        ->where('orderdetails.owner_id',$id)
        ->where('orderdetails.od_status',0);
    })->get();
    return view('user.page.orderpage',['orderall' => $orderall,'od' => $od]);
}); 
///////////////////////////////////////////////////

////////////////// Order Controller ///////////////
Route::get('/save_order','OrderController@save_order');
Route::get('get_allorder', function (){
    $allorder = Order::join('users','orders.owner_id','=','users.id')->orderBy('orders.order_date','DESC')->get();
    return response()->json($allorder);
});
Route::get('/upload_ref',function(Request $request){
    $id = $request->get('id');
    $code = $request->get('code');
    $order = Order::where('order_id',$id)->update(['order_status'=> "ชำระเงินแล้ว",'order_img'=>"pay.png"]);
    $notic = new notification;
    $notic->order_id = $id;
    $notic->notic_detail = "ได้ชำระเงินสินค้ารหัส Order : ".$code ;
    $notic->notic_status = 0;
    $notic->notic_mode = 2;
    $notic->save();
    return response()->json(['success' => 'done']);
});
//////////////////////////////////////////////////

/////////////////////Admin Promotion////////////////
Route::get('save_coupons','PromotionController@save_coupons');
Route::get('del_coupons',function (Request $request){
    $cp_code = $request->get('cp_code');
    Coupons::where('cp_code',$cp_code)->delete();
    return response()->json(['success'=> 'done']);
});
Route::get('getproductby',function (Request $request){
    $product = Product::whereNotIn('p_id',$request->get('pid_selected'))->get();
    return $product;
});
Route::post('save_flashsale','PromotionController@save_flashsale');
Route::get('get_allfs',function(){
    return Flashsale::select('fs_description','fs_key','fs_status','fs_datestart','fs_dateend')->distinct()->get();
});
Route::get('/update_status_fs',function(Request $request){
    $key = $request->get('key');
    $status = $request->get('status');
    $updateall = Flashsale::where('fs_status',1)->update(['fs_status'=>0]);
    $update = Flashsale::where('fs_key',$key)->update(['fs_status'=>$status]);
    return response()->json(['success' => 'done']);
});
Route::delete('delete_fs/{key}',function($key){
    $del = Flashsale::where('fs_key',$key)->delete();
    return response()->json(['success' => 'done']);
});
Route::get('/find_fskey',function(){
    $fs = Flashsale::join('products','flashsales.p_id','=','products.p_id')->get();
    return $fs;
});
Route::get('/testfs', function(){
    $dt = Carbon::now();
    $dstr = $dt->toDateTimeString();
    $nowfs = Flashsale::select('fs_description','fs_key','fs_status','fs_datestart','fs_dateend')
                            ->where('fs_status',1)
                            ->where('fs_datestart','<',$dstr)
                            ->where('fs_dateend','>',$dstr)
                            ->distinct()->get();
    return $nowfs;
});
///////////////// user /////////////////////

Route::get('/user/index',function(){
    return view('user.page.index');
}); 

Route::get('/addtocart',function(Request $request){
    $input = $request->get('param');
    if(isset($input['od_id'])) { 
        $OD = Orderdetail::where('od_id',$input['od_id'])->update(['od_qty' => $input['qty']]);
        return response()->json(['success' => 'done']); 
    }
    else { 
        $OD = new Orderdetail;
        $OD->od_qty = 1;
        $OD->od_status = 0;
        $OD->owner_id = $input['owner'];
        $OD->order_id = 0;
        $OD->p_id = $input['p_id'];
        $OD->save();
        return response()->json(['id' => $OD->id]); 
    }
});

Route::get('/deletecart', function (Request $request) {
        $od_id = $request->get('od_id');
        $del  = Orderdetail::where('od_id',$od_id)->where('od_status', 0)->delete();
        if($del == 1){
           return response()->json(['success' => 'done']); 
        }
        else {
            return response()->json(['success' => 'fail']);
        }
    
});
Route::get('/get_code',function (Request $request){
    $cp_code = $request->get('code');
    $Coupons = Coupons::where('cp_code',$cp_code)->get();
    return $Coupons;
});