<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('pro_id');
            $table->String('p_id');
            $table->text('pro_name');
            $table->text('pro_description');
            $table->integer('pro_mode');
            $table->String('pro_discount');
            $table->date('pro_datestart');
            $table->date('pro_dateend');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
