<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('p_id');
            $table->string('p_code')->nullable();
            $table->string('p_brand_TH');
            $table->string('p_brand_EN')->nullble();
            $table->string('p_name_TH');
            $table->string('p_name_EN')->nullable();
            $table->text('p_detail_TH')->nullable();
            $table->text('p_detail_EN')->nullable();
            $table->string('p_img')->nullable();
            $table->integer('p_price');
            $table->integer('p_discount')->nullable();
            $table->string('p_status')->nullable();
            $table->string('p_url')->nullable();
            $table->string('p_keyword')->nullable();
            $table->text('p_description')->nullable();
            $table->string('p_amount')->nullable();
            $table->integer('c_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
