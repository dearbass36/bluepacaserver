<script src="{{asset('js/admin/slide_right.js')}}"></script>
<link href="../assets/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" />
<style>
    .vakata-context {
        z-index: 1100
    }
    .width{
        width: auto!important;
    }
    .nav-header{
        padding: 10px !important;
        background-color:#1b1f24!important;
    }  
    
</style>
<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
        <!-- begin sidebar scrollbar -->
        <div data-scrollbar="true" data-height="100%">
            <!-- begin sidebar user -->
            <ul class="nav">
                <li class="nav-profile">
                    <a href="javascript:;" data-toggle="nav-profile">
                        <div class="cover with-shadow"></div>
                        <div class="image">
                            <img src="../assets/img/user/user-13.jpg" alt="" />
                        </div>
                        <div class="info">
                            {{ Auth::user()->name }}
                         <small>{{ Auth::user()->email }}</small>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- end sidebar user -->
            <!-- begin sidebar nav -->
            <ul class="nav">
                <li class="nav-header">เมนูหลัก</li>
                <li id="home_menu">
                        <a href="{{url('/admin/home')}}"><i class="fas fa-th-large"></i><span>หน้าแรก</span></a>
                    </li>
                <li id="order_menu">
                    <a href="{{url('/admin/order')}}"><i class="fas fa-shopping-cart"></i><span>จัดการคำสั่งซื้อ</span></a>
                </li>
                <li class="has-sub main-level" id="product_menu">
                    <a data-catid="all">
                        <b class="caret"></b>
                        <i class="fa fa-tags"></i>
                        <span>จัดการสินค้า</span>
                    </a> 
                    <ul class="sub-menu">
                        {!! $catagory !!}
                    </ul>
                </li>
                <li class="nav-header">เมนูโปรโมชั่น</li>
                <li id="promotion_menu">
                    <a href="{{url('/admin/promotion')}}">
                        <i class="fa fa-star"></i>
                        <span>โปรโมชั่น</span>
                    </a>
                </li>
                <li id="coupons_menu">
                    <a href="{{url('/admin/coupons')}}">
                        <i class="fa fa-money-bill-alt"></i>
                        <span>คูปอง</span>
                    </a>
                </li>
                <li id="flashsale_menu">
                    <a href="{{url('/admin/flashsale')}}">
                        <i class="fa fa-bolt"></i>
                        <span>Flash Sale</span>
                    </a>
                </li>
                <li class="nav-header">Blog</li> 
                <li id="blog_menu">
                    <a href="{{url('/admin/blog_manage')}}">
                        <i class="fa fa-newspaper"></i>
                        <span>จัดการข่าว</span>
                    </a>
                </li>    
                <li class="nav-header">Page Layout</li>  
                <!-- begin sidebar minify button -->
                <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
                <!-- end sidebar minify button -->
            </ul>
            <!-- end sidebar nav -->
        </div>
        <!-- end sidebar scrollbar -->
    </div>
    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->

    <!-- Begin Add Category Modal -->
    <div class="modal fade" id="catagoryadd_modal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">จัดการหมวดหมู่</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-7" id="jstree-catagory"></div>
                    <div class="col-lg-5">
                        <div class="text-right">
                            <button type="button" class="btn btn-success btn btn-sm control-btn" onclick="create_cat()"><i class="fas fa-plus-circle fa-fw"></i>Create</button>
                            <button type="button" class="btn btn-yellow btn btn-sm control-btn" onclick="rename_cat()"><i class="fas fa-pencil-alt fa-fw"></i>Rename</button>
                            <button type="button" class="btn btn-danger btn btn-sm control-btn" onclick="delete_cat()"><i class="fas fa-eraser fa-fw"></i>Delete</button>
                        </div>
                        <div id="section_body">
                            <br>
                            <div id="body_create" style="display:none;">
                                <label id="level_text"></label><br>
                                TH :<input type="text" id="createTH_text" class="form-control create_text">
                                EN :<input type="text" id="createEN_text" class="form-control create_text"><br>
                                <div class="text-left">
                                    <button class="btn btn-green form-control width create_conBTN" onclick="crate_click()">confirm</button>
                                    <button class="btn btn-danger form-control width cancel_cat_btn" >cancel</button>
                                </div>
                            </div>
                            <div id="body_rename" style="display:none;">
                                <input type="text" id="renameID_text" hidden>
                                TH :<input type="text" id="renameTH_text" class="form-control">
                                EN :<input type="text" id="renameEN_text" class="form-control"><br>
                                <div class="text-left">
                                    <button class="btn btn-green form-control width" id="rename_conBTN">confirm</button>
                                    <button class="btn btn-danger form-control width cancel_cat_btn">cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button class="btn btn-primary form-control width" onclick="save_cat()">Save Change</button>
            </div>
          </div>
        </div>
    </div>
    <!-- End Add Category Modal-->
	<script src="../assets/plugins/jstree/dist/jstree.min.js"></script>
    