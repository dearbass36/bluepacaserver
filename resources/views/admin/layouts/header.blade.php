<style>
    #menu_notic {
        height: auto!important;
        max-height: 400px!important;
        overflow-y: auto;
    }
    .notread{
        background-color:#edf2fa!important; 
    }
</style>

<!-- begin #header -->
<div id="header" class="header navbar-default">
        <!-- begin navbar-header -->
        <div class="navbar-header">
            <a href="{{ url('/') }}" class="navbar-brand"><span class="navbar-logo"></span> <b>Blue</b> Paca</a>
            <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end navbar-header -->
        <!-- begin header-nav -->
        <ul class="navbar-nav navbar-right">
            <li class="dropdown" ">
                <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14 bell-notic" >
                    <i class="fa fa-bell"></i>
                    @php
                        $count = 0;    
                    @endphp
                    @foreach ($notic as $count_read)
                        @if($count_read->notic_status == 0)
                            @php $count++; @endphp
                        @endif
                    @endforeach
                    @if($count != 0)
                    <span class="label">{{$count}}</span>
                    @endif
                </a>
                <ul class="dropdown-menu media-list dropdown-menu-right" id="menu_notic">
                    <li class="dropdown-header">NOTIFICATIONS</li>
                    @foreach ($notic as $row)
                        @if($row->notic_status == 2)
                            <li class="media" data-notic="{{$row->notic_id}}" data-row="{{$row->order_id}}">
                        @else
                            <li class="media notread" data-notic="{{$row->notic_id}}" data-row="{{$row->order_id}}">
                        @endif
                            <a href="javascript:;" onclick="menunotic_click(this)">
                                <div class="media-left">
                                    @php 
                                        $img_notic = "";
                                        if($row->notic_mode == 1){
                                            $img_notic = "ordernow.png";
                                        }
                                        else if($row->notic_mode == 2){
                                            $img_notic = "pay.png";
                                        }
                                    @endphp
                                    <img src="/picture/{{$img_notic}}" class="media-object" alt="" />
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">{{$row->name}}</h6>
                                    <p>{{$row->notic_detail}}</p>
                                    <div class="text-muted f-s-11">{{$row->notic_date}}</div>
                                </div>
                            </a>
                        </li>
                    @endforeach
                    <li class="dropdown-footer text-center">
                        <a href="/notification">View more</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown navbar-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../assets/img/user/user-13.jpg" alt="" /> 
                    <span class="d-none d-md-inline">{{ Auth::user()->name }}</span> <b class="caret"></b>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="javascript:;" class="dropdown-item">Edit Profile</a>
                    <div class="dropdown-divider"></div>
                <a href="{{ url('logout') }}" class="dropdown-item">Log Out</a>
                </div>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end #header -->
    <script>
        $(function(){
                $('.bell-notic').click(function(){
                    $.get('/update_notic',function(response){
                        $(".bell-notic").html(`<i class="fa fa-bell"></i>`);
                    });
                });
        });
        function refresh_notic(){
            var head = "";
            $.get('/get_notic',function(response){
            
            const fill = response.filter((member) => {
                return  member.notic_status == 0;
            });
            console.log(fill);
            if(fill.length == 0){
            }
            else {
                head = `<span class="label">${fill.length}</span>`;
            };
            $(".bell-notic").html(`<i class="fa fa-bell"></i>${head}`);
            var text = `<li class="dropdown-header">NOTIFICATIONS</li><li class="media">` ;
            for(var i in response){
                var cls,img;
                if(response[i].notic_status == 2 ){
                    cls = "media"
                }
                else{
                    cls = "media notread"
                }
                if(response[i].notic_mode == 1){
                    img = "ordernow.png"
                }
                else if(response[i].notic_mode == 2){
                    img = "pay.png"
                }
                text += `<li class="${cls}" data-notic="${response[i].notic_id}" data-row="${response[i].order_id}">   
                            <a href="javascript:;" onclick="menunotic_click(this)">
                                <div class="media-left">
                                    <img src="/picture/${img}" class="media-object" alt="" />
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">${response[i].name}</h6>
                                    <p>${response[i].notic_detail}</p>
                                    <div class="text-muted f-s-11">${response[i].notic_date}</div>
                                </div>
                            </a>
                        </li>`;
            }
            text += `<li class="dropdown-footer text-center">
                        <a href="/notification">View more</a>
                    </li>`
            $('#menu_notic').html(text);
            });
        };
        function menunotic_click(el){
            var ParentLI = $(el).parent('li');
            var id = ParentLI.data().notic;
            var id_row = ParentLI.data().row;
            $.get('/update_notic_read',{id:id},function(response){
                location.assign("/posts?id="+id_row);
            });
        }
    </script>