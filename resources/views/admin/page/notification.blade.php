<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Notification</title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <link href="../assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet" />
        <link href="../assets/css/default/style.min.css" rel="stylesheet" />
        <link href="../assets/css/default/style-responsive.min.css" rel="stylesheet" />
        <link href="{{asset('lib/css/bootstrap-tokenfield.min.css')}}" rel="stylesheet" />
        <link href="../assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet" />
        <!-- ================== END BASE CSS STYLE ================== -->
            <!-- ================== BEGIN BASE JS ================== -->
            <script src="../assets/plugins/pace/pace.min.js"></script>
            <!-- ================== BEGIN BASE JS ================== -->
        <script src="../assets/plugins/jquery/jquery-3.2.1.min.js"></script>
        <script src="../assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="../assets/plugins/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
        <script src="{{asset('lib/js/sweetalert2.all.min.js')}}"></script>
        <!-- ================== END BASE JS ================== -->
        <style>
           
            i,li {
                cursor: pointer;
            }
            .box-main{
                margin-top:30px;
            }
            .media {
                margin: 0px !important;
                box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.5);
            }
            .notread{
                background-color:#edf2fa!important; 
            }
            .headline{
                margin: 20px;
                border-bottom: 1px solid black;
            }
            .media-objectx{
                height: 50px;
                width: auto!important;
                margin:15px;
                vertical-align:middle;
            }
            .media > a{
                color:black!important;
                background-color: white;
                border-bottom: 1px solid lightgrey;
                
            }
            .block_notic:hover{
                background-color: #f9f9f9!important;
            }
            .media-content{
                height:66 !important ;
                
            }
            .media-heading{
                margin-top:15px!important;
            }
            .label {
                background: #00acac;
                color: #fff;
                border-radius: 20px;
            }
            p {
                margin-bottom:7px!important; 
            }
        </style>
</head>
<body>
    <!-- begin #header -->
<div id="header" class="header navbar-default">
        <!-- begin navbar-header -->
        <div class="navbar-header">
            <a href="{{ url('/') }}" class="navbar-brand"><span class="navbar-logo"></span> <b>Blue</b> Paca</a>
            <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end navbar-header -->
        <!-- begin header-nav -->
        <ul class="navbar-nav navbar-right">
            <li class="dropdown navbar-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../assets/img/user/user-13.jpg" alt="" /> 
                    <span class="d-none d-md-inline">{{ Auth::user()->name }}</span> <b class="caret"></b>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="javascript:;" class="dropdown-item">Edit Profile</a>
                    <div class="dropdown-divider"></div>
                <a href="{{ url('logout') }}" class="dropdown-item">Log Out</a>
                </div>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end #header -->
    <!-- body -->
    <div id="box-main" class="box-main">
        <div class="col-lg-6 offset-3 ">
            <div class="headline">
                <h3>
                    &nbsp;<i class="fa fa-bell"></i> การแจ้งเตือนของคุณ
                </h3>
            </div>
            <div id="notic_body">
                @foreach ($notic as $row)
                    <div class="media" data-notic-id="{{$row->notic_id}}" data-row="{{$row->order_id}}">
                        @if($row->notic_status == 2)
                            <a href="javascript:;" class="block_notic" onclick="menunotic_click(this)">
                        @else
                            <a href="javascript:;" class="block_notic notread" onclick="menunotic_click(this)"> 
                        @endif   
                            <div class="media-content">
                                <div class="media-left">
                                    @php 
                                        $img_notic = "";
                                            if($row->notic_mode == 1){
                                                $img_notic = "ordernow.png";
                                            }
                                            else if($row->notic_mode == 2){
                                                $img_notic = "pay.png";
                                            }
                                            @endphp
                                        <img src="/picture/{{$img_notic}}" class="media-objectx" alt="" />
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">{{$row->name}}</h6>
                                        <p>{{$row->notic_detail}}</p>
                                        <div class="text-muted f-s-11">{{$row->notic_date}}</div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <script>
        const socket = new WebSocket('ws://localhost:8089');
        socket.onopen = function(e) {
            console.log("Connection established!");
        }
        socket.onmessage = function(e){
            refresh_notic()
        }
        function menunotic_click(el){
            var ParentLI = $(el).parent('div');
            var id = ParentLI.data().noticId;
            var id_row = ParentLI.data().row;
            $.get('/update_notic_read',{id:id},function(response){
                location.assign("/posts?id="+id_row);
            });
        }
        function refresh_notic(){
            $.get('/get_notic',function(response){
                var text = "";
                var img,cls;
                for(var i in response){
                    if(response[i].notic_mode == 1){
                        img = "ordernow.png";
                    }
                    else if(response[i].notic_mode == 2){
                        img = "pay.png";
                    }
                    if(response[i].notic_status == 2){
                        cls = "block_notic";
                    }
                    else{
                        cls = "block_notic notread";
                    }
                    text += `<div class="media" data-notic-id="${response[i].notic_id}" data-row="${response[i].order_id}">
                                <a href="javascript:;" class="${cls}" onclick="menunotic_click(this)"> 
                                    <div class="media-content">
                                        <div class="media-left">
                                            <img src="/picture/${img}" class="media-objectx" alt="" />
                                        </div>
                                        <div class="media-body">
                                                <h6 class="media-heading">${response[i].name}</h6>
                                                <p>${response[i].notic_detail}</p>
                                                <div class="text-muted f-s-11">${response[i].notic_date}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>`;    
                }
                $("#notic_body").html(text);
            });
        };
       
    </script>
</body>
</html>