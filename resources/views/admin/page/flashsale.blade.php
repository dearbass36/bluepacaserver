@extends('admin.layouts.layout')
@section('content')
<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
<link href="{{asset('css/admin/flashsale.css')}}" rel="stylesheet">

    <!-- begin #content -->
	<!-- begin #content -->
		<div id="content" class="content">
                  <h1 class="page-header">Flash Sale</h1>
                  <div class="container">
                        <div class="row" id="now_fs">
                              <div class="col-md-3">
                                    <figure class="banner-flashsale">
                                          <img src="{{url('/picture/flash-sale.png')}}">
                                    </figure>
                                    <p class="text-en"> เหลือเวลาอีก </p>
                                    <div class="text-center count_time">
                                          <figure><p>@{{displayTime.hour}}</p></figure>
                                          <figure><p>&#58;</p></figure>
                                          <figure><p>@{{displayTime.min}}</p></figure>
                                          <figure><p>&#58;</p></figure>
                                          <figure><p>@{{displayTime.sec}}</p></figure>
                                    </div>
                              </div>
                              <div class="col-md-9">
                                    <div class="fs-box-show" v-for="fs in fs_show">
                                          <figure class="fs-bg-show">
                                                <img src="/picture/fsbanner.png">
                                                <img :src="/picture/+ fs.p_img ">  
                                          </figure>
                                          <div class="fs-text-show">
                                                <p>@{{fs.p_name_EN}}</p>
                                                <p>@{{fs.p_brand_EN}}</p>
                                                <p>@{{ cal_price(fs.p_price,fs.fs_mode,fs.fs_discount) }} บาท</p>
                                          </div>
                                    </div>
                              </div>
                        </div><br>
                        <div class="row">
                              <h4> - Manage Flash-Sale</h4>
                              <div class="col-md-12">
                                    <ul class="nav nav-tabs">
                                          <li class="nav-items">
                                                <a href="#default-tab-1" data-toggle="tab" class="nav-link active show">
                                                      <span class="d-sm-none">Tab 1</span>
                                                      <span class="d-sm-block d-none">Flash-Sale</span>
                                                </a>
                                          </li>
                                          <li class="nav-items">
                                                <a href="#default-tab-2" data-toggle="tab" class="nav-link">
                                                      <span class="d-sm-none">Tab 2</span>
                                                      <span class="d-sm-block d-none">New Flash-Sale</span>
                                                </a>
                                          </li>
                                    </ul>
                                    <div class="tab-content" id="new_fs">
                                          <!-- begin tab-pane -->
                                          <div class="tab-pane active show" id="default-tab-1">
                                                <table class="table table-bordered text-center" >
                                                      <thead>
                                                            <tr>
                                                                  <th>#</th>
                                                                  <th>Description</th>
                                                                  <th>Date Start</th>
                                                                  <th>Date End</th>
                                                                  <th>Status</th>
                                                                  <th>Other</th>
                                                            </tr>
                                                      </thead>
                                                      <tbody id="MN_body">
                                                            <template v-for="(row,index) in tablerow">
                                                                  <tr >
                                                                        <td>@{{index+1}}</td>
                                                                        <td>@{{row.fs_description}}</td>
                                                                        <td>@{{row.fs_datestart}}</td>
                                                                        <td>@{{row.fs_dateend}}</td>
                                                                        <td>
                                                                              <select class="form-control" v-on:change="Switch_status(index)" v-model="tablerow[index].fs_status">
                                                                                    <option value="0">Unpublish</option>
                                                                                    <option value="1">Publish</option>
                                                                              </select>
                                                                        </td>
                                                                        <td>
                                                                              <button class="btn btn-xs btn btn-info" v-on:click="Seedetail(row.fs_key)">Detail <b class="caret"></b> </button>
                                                                              <button class="btn btn-xs btn btn-danger" v-on:click="Del_fs(index)">Delete</button>
                                                                        </td>
                                                                  </tr>
                                                                  <template v-if="opened.includes(row.fs_key)">
                                                                        <tr class="sub-tab" v-for="(rowdt,index) in detailrow" v-if="rowdt.fs_key == row.fs_key" style="padding:0px;">
                                                                              <td></td>
                                                                              <td><img :src="/picture/ + rowdt.p_img"></td>
                                                                              <td>@{{rowdt.p_brand_EN}}</td>
                                                                              <td>@{{rowdt.p_name_TH}}</td>
                                                                              <td>Price : @{{rowdt.p_price}}</td>
                                                                              <td>Discount : <label>@{{rowdt.fs_discount}}</label>
                                                                                    <label v-if="rowdt.fs_mode == 1">%</label>
                                                                                    <label v-else>THB</label>
                                                                              </td>
                                                                        </tr> 
                                                                  </template>
                                                            </template>
                                                      </tbody>
                                                </table>
                                          </div>
                                          <!-- end tab-pane -->
                                          <!-- begin tab-pane -->
                                          <div class="tab-pane" id="default-tab-2">
                                                <div class="col-md-8 offset-2">
                                                      <div id="accordion" class="card-accordion">
                                                                        <!-- begin card -->
                                                                        <div class="card">
                                                                              <div class="card-header bg-black text-white pointer-cursor collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false">
                                                                                    #1 General 
                                                                              </div>
                                                                              <div id="collapseOne" class="collapse" data-parent="#accordion" style="">
                                                                                    <div class="card-body">
                                                                                          <div class="form-group row">
                                                                                                <label class="col-md-3 col-form-label">Description : </label>
                                                                                                <div class="col-md-9">
                                                                                                      <div class="input-group">
                                                                                                            <input type="text" class="form-control" placeholder="Short Description" id="fs_des">
                                                                                                      </div>
                                                                                                </div>
                                                                                          </div>
                                                                                          <div class="form-group row">
                                                                                                <label class="col-md-3 col-form-label">Date : </label>
                                                                                                <div class="col-md-9">
                                                                                                      <div class="input-group" id="date_form">
                                                                                                                  <input type="text" class="form-control" id="fs_date">
                                                                                                                  <div class="input-group-addon">
                                                                                                                        <i class="fa fa-calendar"></i>
                                                                                                                  </div>
                                                                                                      </div>
                                                                                                </div>
                                                                                          </div>
                                                                                          <div class="form-group row">
                                                                                                <label class="col-md-3 col-form-label">Range : </label>
                                                                                                <div class="col-md-9">
                                                                                                <div class="input-group input-daterange">
                                                                                                      <div class='input-group date' id='range_start' >
                                                                                                            <input type='text' class="form-control" id="fs_start" />
                                                                                                            <span class="input-group-addon">
                                                                                                                  <i class="fa fa-clock"></i>
                                                                                                            </span>
                                                                                                      </div>
                                                                                                      <span class="input-group-addon">to</span>
                                                                                                      <div class='input-group date' id='range_end'>
                                                                                                            <input type='text' class="form-control" id="fs_end"/>
                                                                                                            <span class="input-group-addon">
                                                                                                                  <i class="fa fa-clock"></i>
                                                                                                            </span>
                                                                                                      </div>
                                                                                                </div>
                                                                                                </div>
                                                                                          </div>
                                                                                    </div>
                                                                              </div>
                                                                        </div>
                                                                        <!-- end card -->
                                                                        <!-- begin card -->
                                                                        <div class="card">
                                                                              <div class="card-header bg-black text-white pointer-cursor collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false">
                                                                                    #2 Choose Product
                                                                              </div>
                                                                              <div id="collapseTwo" class="collapse" data-parent="#accordion" style="">
                                                                                    <div class="card-body">
                                                                                         <div class="text-right">
                                                                                                <button class="btn btn-danger btn btn-sm" data-toggle="modal" data-target="#choose_p_modal" id="ADD_btn"><i class="fa fa-plus"></i>&nbsp;ADD</button>    
                                                                                          </div>
                                                                                                <table class="table table-borderless">
                                                                                                      <thead>
                                                                                                            <tr>
                                                                                                                  <th>Product</th>
                                                                                                                  <th>Title</th>
                                                                                                                  <th>Price</th>
                                                                                                                  <th>Condition</th>
                                                                                                                  <th>Discount</th>
                                                                                                                  <th>Other</th>
                                                                                                            </tr>
                                                                                                      </thead>
                                                                                                      <tbody>
                                                                                                            <tr v-for="(rowx,index) in row">
                                                                                                                  <td><img v-bind:src="/picture/ + rowx.p_img" style="max-width:200px;max-height:100px;"></td>
                                                                                                                  <td>@{{rowx.p_brand_EN}} @{{rowx.p_name_TH}}</td>
                                                                                                                  <td>@{{rowx.p_price}}</td>
                                                                                                                  <td>  
                                                                                                                        <select class="form-control" v-model="discount[index].mode">
                                                                                                                              <option value="1">By Percentage(%)</option>
                                                                                                                              <option value="2">By Amount(THB)</option>
                                                                                                                        </select>
                                                                                                                  </td>
                                                                                                                  <td><input type="text" class="form-control" v-model.number="discount[index].discount"></td>
                                                                                                                  <td><button class="btn btn-default" v-on:click="Del_choose(index)"><i class="fas fa-trash-alt fa-fw"></i></button></td> 
                                                                                                            </tr>
                                                                                                      </tbody>
                                                                                                </table>
                                                                                    </div>
                                                                              </div>
                                                                        </div>
                                                                        <!-- end card -->
                                                      </div>
                                                      <!-- end tab-pane -->
                                                </div><br>
                                                <div id="button_newfs" class="button_newfs">
                                                      <button class="btn btn-primary btn btn-sm" v-on:click="Create_click">Create</button>
                                                      <button class="btn btn-default btn btn-sm" v-on:click="Cancel_click">Cancel</button>
                                                </div>
                                          </div>
                                    </div>
                              </div>
                        </div>
            </div>
      <!-- end #content -->

      <div class="modal fade" id="choose_p_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                       <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Choose Product</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                        <div class="modal-body">
                              <div class="form-group row">
                                    <label class="col-md-1 col-form-label">Filter : </label>
                                    <div class="col-md-5">
                                          <input type="text" class="form-control" v-model="search">
                                    </div>
                              </div>
                              <div class="text-center">
                                          <div v-for="(rows,index) in filteredList" class="card-box" :class="{selected : selected_Product.includes(rows.p_id)}">
                                                <a v-on:click="Select_pro(rows.p_id)">
                                                      <figure>
                                                            <img v-bind:src="/picture/ + rows.p_img">
                                                      </figure>
                                                      <div class="text-detail">
                                                            <small class="name">@{{rows.p_name_TH}}</small>
                                                      <p class="brand">@{{rows.p_brand_EN}}</p> 
                                                      </div>
                                                </a>
                                          </div>
                              </div>
                        </div>
                        <div class="modal-footer">
                              <button type="button" id="SAVE_btn" class="btn btn-success" data-dismiss="modal" v-on:click="Save_choose()">Save</button>
                        </div>
                  </div>
            </div>
      </div>

      <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
      <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
      <script src="{{asset('js/admin/flashsale.js')}}"></script>
      <script>
            $(function (){
                  $('#date_form').datetimepicker({
                        format: 'Y-M-D'
                  });
                  $('#range_start').datetimepicker({
                        format: 'HH:mm',
                  });
                  $('#range_end').datetimepicker({
                        format: 'HH:mm',
                  });
            });
      </script>

@endsection