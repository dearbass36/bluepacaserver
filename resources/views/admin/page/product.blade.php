@extends('admin.layouts.layout')
@section('content')

<link href="../assets/plugins/dropzone/min/dropzone.min.css" rel="stylesheet" />
<link href="{{asset('css/admin/product.css')}}" rel="stylesheet" />
<link href="../assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="../assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<script src="{{asset('lib/js/dropzone.min.js')}}"></script>
<script src="{{asset('lib/js/jquery.form.js')}}"></script>
<script src="../assets/plugins/ckeditor/ckeditor.js"></script> 
<script src="{{asset('js/admin/product.js')}}"></script>

		<!-- begin #content -->
		<div id="content" class="content">   
           <!-- begin page-header -->
           <h1 class="page-header">จัดการสินค้า</h1>
           <!-- end page-header -->
              <!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
                <div class="col-lg-12">
                    <!-- begin nav-tabs -->
					<ul class="nav nav-tabs">
                            <li class="nav-items">
                                <a href="#default-tab-1" data-toggle="tab" class="nav-link active">
                                    <span class="d-sm-none">Tab 1</span>
                                    <span class="d-sm-block d-none"><i class="fas fa-archive"></i>&nbsp;สินค้าทั้งหมด</span>
                                </a>
                            </li>
                            <li class="nav-items">
                                <a href="#default-tab-3" data-toggle="tab" class="nav-link">
                                    <span class="d-sm-none">Tab 3</span>
                                    <span class="d-sm-block d-none"><i class="fas fa-cart-plus"></i>&nbsp;เพิ่มสินค้า</span>
                                </a>
                            </li>
                    </ul>
                        <!-- end nav-tabs -->
                        <!-- begin tab-content -->
                        <div class="tab-content">
                            <!-- begin tab-pane -->
                            <div class="tab-pane active show" id="default-tab-1">
                                <div class="panel panel-inverse">
                                        <!-- begin panel-heading -->
                                        <div class="panel-heading">
                                            <div class="panel-heading-btn">
                                                <a href="javascript:;" class="btn btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                        <h3 style="color:white;"><i class="fa fa-truck"></i>&nbsp;รายการสินค้า {{$head}}</h3>
                                        </div>
                                        
                                        <!-- end panel-heading -->
                                        <!-- begin panel-body -->
                                       
                                        <div class="panel-body">
                                            <table id="data-table-default" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="1%" >สินค้า</th>
                                                        <th class="text-nowrap">รหัส</th>
                                                        <th class="text-nowrap">แบรนด์</th>
                                                        <th class="text-nowrap">ชื่อ</th>
                                                        <th class="text-nowrap">ราคา ( บาท )</th>
                                                        <th class="text-nowrap">จำนวน</th>
                                                        <th width="1%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="t_body">
                                                @foreach($product as $value)
                                                    <tr class="odd gradeX" data-id="{{$value->p_id}}">
                                                        <td class="with-img text-center">
                                                            @if($value->p_img == null)
                                                                @php 
                                                                    $photo = "drag.jpg";
                                                                @endphp
                                                            @else
                                                                @php
                                                                    $photo = $value->p_img;
                                                                @endphp
                                                            @endif
                                                            <img src="{{asset("picture/$photo")}}" class="img-rounded" height="70px" width="100"/>
                                                        </td>
                                                        <td data-row="code"><span>{{$value->p_code}}</span> &nbsp;<a class="duplicate_btn" data-toggle="tooltip" title="คัดลอก"><i class="fas fa-copy fa-lg"></i></a></td>
                                                        <td data-row="brand" class="text-center">
                                                            <div class="wrap-control">
                                                                <p data-lang="TH" class="brandTH_txt">{{$value->p_brand_TH}}</p>
                                                                <p data-lang="ENG" class="brandEN_txt" style="display:none;">{{$value->p_brand_EN}}</p>
                                                                <a data-lang="TH" class="btn btn-xs btn-icon btn-circle btn-default lang_btn active">TH</a>
                                                                <a data-lang="ENG" class="btn btn-xs btn-icon btn-circle btn-default lang_btn">ENG</a>   
                                                            </div>
                                                            <div class="wrap-option">
                                                                <input type="text" class="form-control brandTH_edit input-table" placeholder="แบรนด์" value="{{$value->p_brand_TH}}" >
                                                                <input type="text" class="form-control brandEN_edit input-table" placeholder="brand" value="{{$value->p_brand_EN}}" >    
                                                            </div>
                                                        </td>
                                                        <td data-row="name" class="text-center">
                                                            <div class="wrap-control">
                                                                <p data-lang="TH" class="nameTH_txt">{{$value->p_name_TH}}</p>
                                                                <p data-lang="ENG"  class="nameEN_txt" style="display:none;">{{$value->p_name_EN}}</p>
                                                                <a data-lang="TH" class="btn btn-xs btn-icon btn-circle btn-default lang_btn active">TH</a>
                                                                <a data-lang="ENG" class="btn btn-xs btn-icon btn-circle btn-default lang_btn">ENG</a>
                                                            </div>
                                                            <div class="wrap-option">
                                                                <input type="text" class="form-control nameTH_edit input-table"  placeholder="ชื่อสินค้า" value="{{$value->p_name_TH}}" ><br>
                                                                <input type="text" class="form-control nameEN_edit input-table"  placeholder="Product Name" value="{{$value->p_name_EN}}" >
                                                            </div>
                                                            
                                                        </td>
                                                        <td data-row="price">
                                                            <div class="wrap-control">
                                                                <p class="price_txt">{{$value->p_price}}</p>
                                                            </div>
                                                            <div class="wrap-option">
                                                                <input type="text" class="form-control price_edit" value="{{$value->p_price}}" size="5">
                                                            </div>
                                                           
                                                        </td>
                                                        <td data-row="amount">
                                                            <div class="wrap-control">
                                                                <p class="amount_txt">{{$value->p_amount}}</p>
                                                            </div>
                                                            <div class="wrap-option">
                                                                <input type="text" class="form-control amount_edit" value="{{$value->p_amount}}" size="5">
                                                            </div>
                                                           
                                                        </td>
                                                        <td>
                                                            <br>
                                                                <div class="wrap-control">
                                                                    <a  class="edit_btn"><i class="fas fa-pencil-alt fa-fw"></i></a>
                                                                    <a  class="del_btn"><i class="fas fa-trash-alt fa-fw"></i></a>
                                                                </div>
                                                                <div class="wrap-option">
                                                                    <a  class="edit_save green" ><i class="fas fa-check fa-fw"></i></a>
                                                                    <a  class="edit_back red" ><i class="fas fa-times fa-fw"></i></a>
                                                                </div>
                                                                
                                                                <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown"><i class="fas fa-cog fa-fw"></i></a>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <li><a class="property_btn"><i class="fas fa-sticky-note fa-fw"></i> คุณสมบัติ</a></li>
                                                                    <li><a class="discount_btn"><i class="fas fa-money-bill-alt fa-fw"></i> &nbsp;ส่วนลด</a></li>
                                                                    <li><a class="img_btn"><i class="fas fa-image fa-fw"></i> รูปภาพ</a></li>
                                                                    <li><a class="other_btn"><i class="fas fa-info-circle fa-fw"></i> อื่นๆ</a></li>
                                                                </ul>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- end panel-body -->
                                    </div>
                            </div>
                            <!-- end tab-pane -->
                            <!-- begin tab-pane -->
                            <div class="tab-pane " id="default-tab-3">
                                <div class="row">
                                <!-- div col6 -->
                                <div class="col-lg-6">
                                    <!-- begin nav-pills -->
                                    <ul class="nav nav-pills">
                                            <li class="nav-items">
                                                <a href="#nav-pills-tab-1" data-toggle="tab" class="nav-link active">
                                                    <span class="d-sm-none">Pills 1</span>
                                                    <span class="d-sm-block d-none"><i class="flag-icon flag-icon-us"></i> &nbsp;EN</span>
                                                </a>
                                            </li>
                                            <li class="nav-items">
                                                <a href="#nav-pills-tab-2" data-toggle="tab" class="nav-link">
                                                    <span class="d-sm-none">Pills 2</span>
                                                    <span class="d-sm-block d-none "><i class="flag-icon flag-icon-th"></i> &nbsp;TH</span>
                                                </a>
                                            </li>
                                            <li class="nav-items">
                                                <a href="#nav-pills-tab-3" data-toggle="tab" class="nav-link">
                                                    <span class="d-sm-none">Pills 5</span>
                                                    <span class="d-sm-block d-none"><i class="fa fa-info"></i>&nbsp;OTHER</span>
                                                </a>
                                            </li>
                                            <li class="nav-items">
                                                <a href="#nav-pills-tab-4" data-toggle="tab" class="nav-link">
                                                    <span class="d-sm-none">Pills 3</span>
                                                    <span class="d-sm-block d-none"><i class="fab fa-twitter"></i>&nbsp;SEO</span>
                                                </a>
                                            </li>
                                            <li class="nav-items">
                                                <a href="#nav-pills-tab-5" data-toggle="tab" class="nav-link">
                                                    <span class="d-sm-none">Pills 4</span>
                                                    <span class="d-sm-block d-none"><i class="fa fa-camera"></i>&nbsp;MEDIA</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <!-- end nav-pills -->
                                        <!-- begin tab-content -->
                                        <div class="tab-content">
                                            <!-- begin tab-pane -->
                                            <div class="tab-pane active show" id="nav-pills-tab-1">
                                                <h4 class="m-t-10">Product info</h4>
                                                <hr><br>
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">Brand : </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="brand_add_EN"  id="brand_add_EN" class="form-control"/>
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">Name : </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="name_add_EN" id="name_add_EN" class="form-control"/>
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-12">
                                                    <label class="col-md-2 col-form-label text-md-right">Property : </label>
                                                    <div class="col-md-10">
                                                        <textarea class="ckeditor" id="prop_add_EN" name="prop_add_EN" rows="5" ></textarea>
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                            </div>
                                            <!-- end tab-pane -->
                                            <!-- begin tab-pane -->
                                            <div class="tab-pane " id="nav-pills-tab-2">
                                                <h4 class="m-t-10">ข้อมูลสินค้า</h4>
                                                <hr><br>
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">แบรนด์ : </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="brand_add_TH" id="brand_add_TH"  class="form-control"/>
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">ชื่อ : </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="name_add_TH"  id="name_add_TH" class="form-control"/>
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                                 <!-- begin form-group -->
                                                 <div class="form-group row m-b-12">
                                                    <label class="col-md-2 col-form-label text-md-right">คุณสมบัติ : </label>
                                                    <div class="col-md-10">
                                                        <textarea class="ckeditor" id="prop_add_TH" name="prop_add_TH" rows="5" ></textarea>
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                            </div>
                                            <!-- end tab-pane -->
                                            <!-- begin tab-pane -->
                                            <div class="tab-pane " id="nav-pills-tab-3">
                                                <h4 class="m-t-10">หมวดหมู่และราคา</h4>
                                                <hr><br>
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">รหัสสินค้า : </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="code_add" id="code_add" class="form-control" data-parsley-group="step-3" data-parsley-required="true" data-parsley-type="alphanum" />
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">หมวดหมู่ : </label>
                                                    <div class="col-md-10">
                                                        <div class="row row-space-6">
                                                            <div class="col-4">
                                                                <select class="form-control" id="level1_add" name="level1_add">
                                                                    <option value=" ">- LEVEL1 -</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-4">
                                                                <select class="form-control" id="level2_add" name="level2_add">
                                                                    <option value=" ">- LEVEL2 -</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-4">
                                                                <select class="form-control" id="level3_add" name="level3_add">
                                                                    <option value=" ">- LEVEL3 -</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">ราคาสินค้า: </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="price" placeholder="THB." id="price" class="form-control" />
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                            </div>
                                            <!-- end tab-pane -->
                                            <!-- begin tab-pane -->
                                            <div class="tab-pane " id="nav-pills-tab-4">
                                                <h4 class="m-t-10">การทำ SEO เเละ Keyword</h4>
                                                <hr><br>
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">ลิงค์ (URL) : </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="url_add" id="url_add" placeholder="Ex : goinglabs.com/products/XXXXXXXXXX" class="form-control" data-parsley-group="step-3" data-parsley-required="true" />
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                                <!-- begin form-group -->
                                                <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">คำอธิบาย : </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="description_add" id="description_add" placeholder="คำอธิบายแบบสั้น" class="form-control" data-parsley-group="step-3" data-parsley-required="true" />
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                                 <!-- begin form-group -->
                                                 <div class="form-group row m-b-10">
                                                    <label class="col-md-2 col-form-label text-md-right">คีย์เวิร์ด : </label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="key_add" id="key_add" class="form-control" data-parsley-group="step-3" data-parsley-required="true" />
                                                    </div>
                                                </div>
                                                <!-- end form-group -->
                                            </div>
                                            <!-- end tab-pane -->
                                            <!-- begin tab-pane -->
                                            <div class="tab-pane " id="nav-pills-tab-5">
                                                <h4 class="m-t-10">รูปภาพรอง</h4>
                                                <hr><br>
                                                <div id="dropzone">
                                                        <form action="/upload" class="dropzone " id="my-dropzone" enctype="multipart/form-data" method="post">
                                                            {{ csrf_field() }}
                                                            <div class="dz-message needsclick">
                                                                    ดรอปไฟล์ <b>ลงที่นี่</b> หรือ <b>คลิก</b> เพื่ออัพโหลด<br />
                                                            </div>
                                                        </form>
                                                        <input text="text" value="" id="sub_img" hidden>
                                                </div>
                                            </div>
                                            <!-- end tab-pane -->
                                            <div id="alert_msg">

                                            </div>
                                           
                                    </div>
                                    <!-- end tab-content -->
                                </div>
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5 text-right"> 
                                        <button type="button" class="btn btn-green btn-sm" id="save_btn"><i class="fas fa-save"></i>&nbsp;Save</button>
                                        <button type="button" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i>&nbsp;Preview</button>
                                        <button type="button" class="btn btn-yellow btn-sm"><i class="fas fa-edit"></i>&nbsp;Clear</button>
                                        <button type="button" class="btn btn-danger btn-sm"><i class="fas fa-eraser"></i>&nbsp;Cancel</button>
                                        <br><br>
                                            <div class="form-group text-left">
                                                <label >รูปภาพหลัก </label>
                                                <div class="text-center" id="dropped">
                                                     <form method="POST" action="" id="Upload_mainphoto" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <img class="blah" src="{{asset('picture/drag.jpg')}}" alt="your image" width="250" height="200" onclick="main_file.click()" />
                                                        <br><br>
                                                        <input type="file" class="form-control" name="main_file" id="main_file" onchange="readURL(this);" style="display:none"> 
                                                        <span ><a href="javascript:;" style="color:orange;" onclick="main_file.click()">*คลิกเพื่ออัพโหลด</small></a>     
                                                    </form>  
                                                    <input text="text" value="" id="main_img" hidden>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                        <!-- begin #accordion -->
                                                        <div id="accordion" class="card-accordion">
                                                            <!-- begin card -->
                                                            <div class="card">
                                                                <div class="card-header bg-black text-white pointer-cursor" data-toggle="collapse" data-target="#collapseOne">
                                                                    โปรโมชั่นและการลดราคา
                                                                </div>
                                                                <div id="collapseOne" class="collapse text-left" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        <div class="form-group">
                                                                            <label for="label-discount">ส่วนลด ( % )</label> 
                                                                            <input type="text" class="form-control" id="discount" name="discount"> 
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label for="label-startdate">ตั้งแต่</label> 
                                                                                <input type="date" class="form-control" id="date_start" name="date_start"> 
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label for="label-startdate">จนถึง</label> 
                                                                                <input type="date" class="form-control" id="date_end" name="date_end"> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end card -->
                                                            <!-- begin card -->
                                                            <div class="card">
                                                                <div class="card-header bg-black text-white pointer-cursor collapsed" data-toggle="collapse" data-target="#collapseTwo">
                                                                    สถานะสินค้า
                                                                </div>
                                                                <div id="collapseTwo" class="collapse text-left" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        <div class="form-group">
                                                                            <label for="status">สถานะสินค้า</label>
                                                                            <select class="form-control" id="status_pro" name="status_pro">
                                                                                  <option value=" ">กรุณาเลือก</option> 
                                                                                  <option value="สินค้าใหม่">สินค้าใหม่</option>
                                                                                  <option value="สินค้าขายดี">สินค้าขายดี</option> 
                                                                                  <option value="สินค้าเเนะ">สินค้าเเนะนำ</option>
                                                                                  <option value="สินค้าปกติ">สินค้าปกติ</option>
                                                                                  <option value="ปิดสินค้า">ปิดสินค้า</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end card -->
                                                        </div>
                                                        <!-- end #accordion -->  
                                                </div>
                                            </div>
                                </div>
                                </div>
                            </div>
                            <!-- end tab-pane -->
                        </div>
                        <!-- end tab-content -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
         </div>
             <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/plugins/js-cookie/js.cookie.js"></script>
	<script src="../assets/js/theme/default.min.js"></script>
	<script src="../assets/js/apps.min.js"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->
    
	<script src="../assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="../assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="../assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="../assets/js/demo/table-manage-default.demo.min.js"></script>

    <!-- Dropzone config -->
    <script>
            var dropzone_files = [] ;
            Dropzone.options.myDropzone = {
                addRemoveLinks: true,
                dictRemoveFile: 'Remove file',
                dictFileTooBig: 'Image is larger than 16MB',
                timeout: 10000,
                init: function () {
                    this.on("removedfile", function (file) {
                        id = "";
                        for(var i in dropzone_files){
                            if( dropzone_files[i].old_name == file.name ){
                                id = dropzone_files[i].new_name;
                                 dropzone_files.splice(i,1);
                            }
                        }
                        var text = "";
                        for(var i in dropzone_files){
                                    if(i == 0){
                                        text += dropzone_files[i].new_name;
                                    }
                                    else{
                                        text += ","+dropzone_files[i].new_name;
                                    }
                                    
                                }
                                $("#sub_img").val(text)
                        var param = {
                            id: id
                        }
                        $.get({
                            url: '/delete_photo',
                            data: param,
                            dataType: 'json',
                            success: function (data) {
                            }
                        });
                    });
                },
                success: function (file, done) {
                    dropzone_files.push({'new_name': done, 'old_name': file.name});
                    var text = "";
                    for(var i in dropzone_files){
                        if(i == 0){
                            text += dropzone_files[i].new_name;
                        }
                        else{
                            text += ","+dropzone_files[i].new_name;
                        }
                        
                    }
                    $("#sub_img").val(text)
                }
            };
    </script>
    <script>
        TableManageDefault.init();
    </script>
    <!-- End Dropzone -->
    <!-- CK Config -->
        <script>
            var options = {
                            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
                        };

        CKEDITOR.replace('prop_add_EN', options)
        CKEDITOR.replace('prop_add_TH', options)
        </script>
    <!-- End CK -->
<!-- Modal Property -->
    <div class="modal fade" id="modal_property" >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"><i class="fas fa-sticky-note fa-fw"></i>&nbsp;คุณสมบัติ</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <!-- begin nav-tabs -->
					<ul class="nav nav-tabs" style="background-color:#242a30!important;">
						<li class="nav-items">
							<a href="#property_TH" data-toggle="tab" class="nav-link active">
								<span class="d-sm-none">Tab 1</span>
								<span class="d-sm-block d-none"><i class="flag-icon flag-icon-th"></i> &nbsp;TH</span>
							</a>
						</li>
						<li class="nav-items">
							<a href="#property_EN" data-toggle="tab" class="nav-link">
								<span class="d-sm-none">Tab 2</span>
								<span class="d-sm-block d-none"><i class="flag-icon flag-icon-us"></i> &nbsp;EN</span>
							</a>
                        </li>
                    </ul>
                <!-- end nav-tabs --><br>
               
                    <div class="tab-content">
                            <div class="text-right container wrap-control">
                                    <a class="property_edit_btn"><i class="fas fa-pencil-alt fa-2x"></i></a>
                            </div>
                            <div class="text-right container wrap-option">
                                   <a  class="property_edit_save green" ><i class="fas fa-check fa-2x"></i></a>&nbsp;
                                   <a  class="property_edit_back red" ><i class="fas fa-times fa-2x"></i></a>
                            </div>
                            <div id="property_TH" class="tab-pane active">
                                <div class="wrap-control" id="propertyTH_edit_text">

                                </div>
                                <div class="wrap-option">
                                    <br>
                                    <textarea class="ckeditor" id="propTH_edit_area" name="propTH_edit_area" rows="5" ></textarea>
                                </div>
                                
                            </div>
                            <div id="property_EN" class="tab-pane ">
                                <div class="wrap-control" id="propertyEN_edit_text">

                                </div>
                                <div class="wrap-option">
                                    <br>
                                    <textarea class="ckeditor" id="propEN_edit_area" name="propEN_edit_area" rows="5" ></textarea>
                                </div>
                            </div>
                    </div>
            </div>
          </div>
        </div>
    </div>
<!-- End Modal Property -->

<!-- Modal Discount -->
    <div class="modal fade" id="modal_discount">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"><i class="fas fa-money-bill-alt fa-fw"></i>&nbsp;โปรโมชั่น</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="form-group">
                        <div class="row">
                            <label >ส่วนลด : </label>
                            <input type="text" id="discount_edit_text" class="form-control" placeholder="%">
                        </div><br>
                        <div class="row">
                            <label >เริ่มวันที่ : </label>
                            <input type="date" id="start_edit_text" class="form-control">
                        </div><br>
                        <div class="row">
                            <label >จนถึงวันที่ : </label>
                            <input type="date" id="end_edit_text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-green discount_save_btn">save</button>
            </div>
          </div>
        </div>
    </div>
<!-- End Modal Discount -->

<!-- Modal Img -->
    <div class="modal fade" id="modal_img">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"><i class="fas fa-image fa-fw"></i>&nbsp;รูปภาพ</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
            <form action="{{ route('editImgUpload')}}" method="POST" id="modal-upimg" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="text" id="id_modalimg" name="id_modalimg" hidden>
                    <input type="text" id="id_removeimg" name="id_removeimg" hidden>
                    <strong>รูปภาพหลัก</strong><br><br>
                    <div id="img_main" class="text-center">
                        <img id="mainmodal_img" src="" alt="your image" width="230" height="180" onclick="mainmodal_file.click()" />
                        <br><br>
                        <input type="file" class="form-control" name="main_file" id="mainmodal_file" value="" onchange="readURL2(this);" style="display:none"> 
                        <span ><a href="javascript:;" style="color:orange;" onclick="mainmodal_file.click()">*คลิกเพื่ออัพโหลด</small></a>        
                    </div>
                    <hr>
                    <strong>รูปภาพรอง</strong>
                    <div class="text-right">
                        <a onclick="add_subimg(this)"><i class="fas fa-plus-square fa-2x"></i></a>
                    </div>
                    <div id="img_sub" class="text-center container">
                       
                    </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <input type="submit" id="editsaveimg_btn" class="btn btn-green" value="save">
            </div>
            </form>
          </div>
        </div>
    </div>
<!-- End Modal Img -->
<script>
    CKEDITOR.replace('propTH_edit_area', options)
    CKEDITOR.replace('propEN_edit_area', options)
</script>

<!-- Modal Other -->
    <div class="modal fade" id="modal_other">
        <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
            <h4 class="modal-title"><i class="fas fa-info-circle fa-fw"></i>&nbsp;อื่นๆ</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                    <div class="form-group row m-b-10">
                        <label class="col-md-2 col-form-label text-md-right">หมวดหมู่ : </label>
                        <div class="col-md-10">
                            <div class="row row-space-6">
                                <div class="col-4">
                                    <select class="form-control level_edit" id="level1_edit" data-level="1">
                                        <option value=" ">- LEVEL1 -</option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select class="form-control level_edit" id="level2_edit" data-level="2">
                                        <option value=" ">- LEVEL2 -</option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select class="form-control level_edit" id="level3_edit" data-level="3">
                                        <option value=" ">- LEVEL3 -</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label class="col-md-2 col-form-label text-md-right">ลิงค์(URL) : </label>
                        <div class="col-md-10">
                            <input type="text" name="url_edit" id="url_edit" placeholder="Ex : goinglabs.com/products/XXXXXXXXXX" class="form-control" data-parsley-group="step-3" data-parsley-required="true" />
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label class="col-md-2 col-form-label text-md-right">คำอธิบาย : </label>
                        <div class="col-md-10">
                            <input type="text" name="description_edit" id="description_edit" placeholder="คำอธิบายแบบสั้น" class="form-control" data-parsley-group="step-3" data-parsley-required="true" />
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label class="col-md-2 col-form-label text-md-right">คีย์เวิร์ด : </label>
                        <div class="col-md-10">
                            <input type="text" name="key_edit" id="key_edit" class="form-control" data-parsley-group="step-3" data-parsley-required="true" />
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label class="col-md-2 col-form-label text-md-right">สถานะ : </label>
                        <div class="col-md-10">
                                <select class="form-control" id="status_edit" name="status_edit">
                                        <option value="สินค้าใหม่">สินค้าใหม่</option>
                                        <option value="สินค้าขายดี">สินค้าขายดี</option> 
                                        <option value="สินค้าเเนะ">สินค้าเเนะนำ</option>
                                        <option value="สินค้าปกติ">สินค้าปกติ</option>
                                        <option value="ปิดสินค้า">ปิดสินค้า</option>
                                </select>
                        </div>
                    </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button class="btn btn-green" id="saveother_btn">save</button>
            </div>   
            </form>
        </div>
        </div>
    </div>
<!-- End Modal Other -->
@endsection