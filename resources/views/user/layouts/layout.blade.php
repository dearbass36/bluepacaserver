<!DOCTYPE html>
<html lang="en">

<head>

    <title>GDS Base Theme</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
    <link href="../assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet" />
    <link href="../assets/plugins/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" />
    <!-- CSS -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="../assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="../assets/plugins/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" />
	<link href="{{asset('lib/css/bootstrap-tokenfield.min.css')}}" rel="stylesheet" />
	<link href="../assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	<script src="../assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="../assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="../assets/plugins/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<script src="{{asset('lib/js/sweetalert2.all.min.js')}}"></script>
	<script src="{{asset('lib/js/bootstrap-tokenfield.min.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
	<style>
       a:hover,a{
           color: black;
       }
		button ,a{
			cursor: pointer;
		}
        .box {
            width: 300px;
            border: 1px solid gray;
            border-radius: 5px;
            margin : 25px;
            display: inline-block;
        
        }
        .box figure {
            padding: 10px;
            border-bottom: 1px solid gray;
            margin: 0 10px 15px;
        }
        .box_img {
            max-width: 100%;
            max-height: 150px;
            display: block;
            margin: 0 auto;
        }
        .box_link {
            display: block;
            text-align: center;
            width: 80%;
            border: 1px;
            border-radius: 5px;
            color: white;
            padding: 5px;
            background-color: #348ee3!important;
            margin: 0 auto;
        }
        .box_body {
            text-align: center;
            padding: 0 20px 20px;
        }
        .title {
            height: 40px;
            overflow: hidden;
        }
        .red{
            color:red;
        }
        tr,td{
            vertical-align: middle!important;
            text-align: center!important;
            background-color:white !important; 
        }
        input{
            width:50px;
        }

	</style>
   <style>
       body { 
           font-family : 'Kanit';
       }
   </style>
   <script>
       var socket = new WebSocket('ws://localhost:8089');
                socket.onopen = function(e) {
                    console.log("Connection established!");
                }
   </script>
<body style="margin:0!important">
    
@include('user.layouts.header')

@yield('content')
    
@include('user.layouts.footer')


    <!-- Optional JavaScript -->
    
<script>
    function server_send(){
        socket.send("Progress");
    }
</script>
</body>

</html>